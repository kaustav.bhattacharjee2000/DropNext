import React, { useState } from "react"
import { makeStyles } from "@material-ui/core/styles"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import ListItemAvatar from "@material-ui/core/ListItemAvatar"
import Avatar from "@material-ui/core/Avatar"
import ArrowBackIosRoundedIcon from "@material-ui/icons/ArrowBackIosRounded"
import CircularProgress from "@material-ui/core/CircularProgress"
import Box from "@material-ui/core/Box"

import { useAuth } from "../contexts/auth-context"
import { useNotification } from "../contexts/notification-context"
import DarkModeToggle from "./DarkModeToggle"
import SoundsToggle from "./SoundsToggle"
import oneTheme from "../utils/theme"
import {
  MAX_DISPLAY_NAME_LENGTH,
  ALLOWED_DISPLAY_NAME_CHARS,
} from "../utils/constants"

const useStyles = makeStyles(theme => ({
  root: {
    color: oneTheme.settings.sectionText[theme.props.mode],
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  pageTitle: {
    fontSize: "1.3rem",
    fontWeight: "bold",
    color: "rgba(0,0,0,0.7)",
    marginBottom: theme.spacing(2),
  },
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  value: {
    fontWeight: "bold",
  },
  section: {
    backgroundColor: oneTheme.settings.sectionBackground[theme.props.mode],
    margin: theme.spacing(0, 0, 4, 0),
    padding: theme.spacing(2),
    borderRadius: theme.spacing(1),
  },
  userCard: {
    paddingLeft: 0,
    paddingRight: 0,
    marginBottom: theme.spacing(1),
    color: oneTheme.settings.profileText[theme.props.mode],
  },
  displayName: {
    fontSize: 18,
    fontWeight: "bold",
  },
  backbtnWrapper: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    columnGap: theme.spacing(2),
    color: oneTheme.settings.profileText[theme.props.mode],
  },
  backbtn: {
    backgroundColor: oneTheme.settings.sectionBackground[theme.props.mode],
    padding: theme.spacing(1),
    borderRadius: theme.spacing(1),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    margin: theme.spacing(1, 0),
    boxShadow: "0 4px 20px -8px rgba(0,0,0,0.4)",
    cursor: "pointer",
  },
  backicon: {
    height: theme.spacing(2),
    width: theme.spacing(2),
  },
  bodyWrapper: {
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      maxWidth: 540,
    },
  },
  edit: {
    padding: theme.spacing(0.5, 1),
    borderRadius: 5,
    color: "white",
    cursor: "pointer",
    marginLeft: theme.spacing(4),
    backgroundColor: oneTheme.settings.editBackground[theme.props.mode],
  },
  textBoxContainer: {
    flexGrow: 1,
  },
  textBoxInput: {
    width: "100%",
    boxSizing: "border-box",
    backgroundColor: "transparent",
    padding: theme.spacing(0.75, 1),
    marginTop: theme.spacing(1),
    outline: "none",
    borderRadius: 3,
    border: oneTheme.joinPage.inputBoxBorder[theme.props.mode],
    borderWidth: 1,
    fontSize: 16,
    fontFamily: "Nunito Sans",
    color: oneTheme.joinPage.text[theme.props.mode],
  },
  details: {},
}))

function GoBack({ hideSettings }) {
  const classes = useStyles()

  return (
    <Box className={classes.backbtnWrapper}>
      <Box className={classes.backbtn} onClick={hideSettings}>
        <ArrowBackIosRoundedIcon className={classes.backicon} />
      </Box>
      <Typography>Go back</Typography>
    </Box>
  )
}

function Settings(props) {
  const classes = useStyles()
  const { user, updateDisplayName } = useAuth()
  const { showNotification } = useNotification()
  const [editing, setEditing] = useState(false)
  const [updatingDB, setUpdatingDB] = useState(false)

  const validateDisplayName = displayName => {
    let errorMessage = null

    if (displayName.length > MAX_DISPLAY_NAME_LENGTH) {
      errorMessage = `Display name can be at max ${MAX_DISPLAY_NAME_LENGTH} characters`
    }
    if (ALLOWED_DISPLAY_NAME_CHARS.test(displayName)) {
      errorMessage = "Only alphabets, numbers, _ is allowed in display name"
    }

    return errorMessage
  }

  const setNewDisplayName = async event => {
    let newName = event.target.value
    if (newName.length > 0) {
      try {
        // Validate display name
        const errorMessage = validateDisplayName(newName)
        if (errorMessage) {
          showNotification({ type: "warn", message: errorMessage })
          return null
        }

        // Try updating db
        setUpdatingDB(true)
        await updateDisplayName(newName)
      } catch (error) {
        console.error("On settings page -> ", error)
        showNotification({ type: "error", message: error.message })
      } finally {
        setUpdatingDB(false)
      }
    }
    setEditing(false)
  }

  const enableEditing = () => {
    setEditing(true)
  }

  return (
    <Box className={classes.root}>
      <GoBack hideSettings={props.hideSettings} />
      <Box className={classes.bodyWrapper}>
        <List>
          <ListItem className={classes.userCard}>
            <ListItemAvatar style={{ marginRight: 10 }}>
              <Avatar
                alt="my avatar"
                src={user.avatar}
                className={classes.avatar}
              />
            </ListItemAvatar>
            <ListItemText
              primary={
                <Typography className={classes.displayName}>
                  {user.displayName}
                </Typography>
              }
              secondary={
                <Typography style={{ fontSize: 14 }}>{user.email}</Typography>
              }
            />
          </ListItem>
        </List>

        <Grid container>
          <Grid item container xs={12} className={classes.section}>
            <Grid item container spacing={2}>
              <Grid item xs={12}>
                <Box
                  display="flex"
                  justifyContent="space-between"
                  alignItems="center"
                >
                  <Box flex={1}>
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      alignItems="center"
                    >
                      <Typography>Display Name</Typography>
                      {updatingDB && (
                        <CircularProgress size={20} color="inherit" />
                      )}
                    </Box>
                    {!editing ? (
                      <Typography className={classes.value}>
                        {user.displayName}
                      </Typography>
                    ) : (
                      <Box className={classes.textBoxContainer}>
                        <input
                          className={classes.textBoxInput}
                          placeholder={user.displayName}
                          onBlur={setNewDisplayName}
                          name="name"
                          autoFocus
                        />
                      </Box>
                    )}
                  </Box>
                  {!editing && (
                    <Box className={classes.edit} onClick={enableEditing}>
                      Edit
                    </Box>
                  )}
                </Box>
              </Grid>

              <Grid item xs={12} className={classes.details}>
                <Typography>Email</Typography>
                <Typography className={classes.value}>{user.email}</Typography>
              </Grid>

              <Grid item xs={12} className={classes.details}>
                <Typography>Google Authenticated</Typography>
                <Typography className={classes.value}>
                  {user.isGoogleAuthenticated ? "Yes" : "No"}
                </Typography>
              </Grid>

              <Grid item xs={12} className={classes.details}>
                <Typography>Joined On</Typography>
                <Typography className={classes.value}>
                  {user.joinedOn.toDate().toLocaleDateString()}
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item container xs={12} className={classes.section}>
            <Grid item container xs={12} spacing={2}>
              <Grid item xs={12}>
                <Box
                  display="flex"
                  justifyContent="space-between"
                  alignItems="center"
                >
                  <Typography>Background theme</Typography>
                  <DarkModeToggle />
                </Box>
              </Grid>

              <Grid item xs={12}>
                <Box
                  display="flex"
                  justifyContent="space-between"
                  alignItems="center"
                >
                  <Typography>Notification sounds</Typography>
                  <SoundsToggle />
                </Box>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </Box>
  )
}

export default Settings
