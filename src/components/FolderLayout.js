import React, { useState, useRef } from "react"
import { navigate } from "gatsby"
import { useLocation } from "@reach/router"
import AppBar from "@material-ui/core/AppBar"
import IconButton from "@material-ui/core/IconButton"
import MenuIcon from "@material-ui/icons/Menu"
import SearchIcon from "@material-ui/icons/Search"
import ExpandMoreIcon from "@material-ui/icons/ExpandMore"
import ExpandLessIcon from "@material-ui/icons/ExpandLess"
import Toolbar from "@material-ui/core/Toolbar"
import CssBaseline from "@material-ui/core/CssBaseline"
import InputBase from "@material-ui/core/InputBase"
import Paper from "@material-ui/core/Paper"
import Divider from "@material-ui/core/Divider"
import { makeStyles } from "@material-ui/core/styles"
import { useQuery, useMutation, useQueryClient } from "react-query"
import clsx from "clsx"

import Settings from "./Settings"
import SideBar from "./SideBar"
import Search from "./Search"
import Path from "./Path"
import FolderContents from "./FolderContents"
import FolderModal from "./FolderModal"
import * as FolderAPI from "../api/folder"
import { useAuth } from "../contexts/auth-context"
import { useNotification } from "../contexts/notification-context"
import { simplifyFileSize } from "../utils/utils"
import oneTheme from "../utils/theme"
import {
  SIDEBAR_WIDTH,
  MAX_UPLOAD_FILE_SIZE,
  MAX_FNAME_LENGTH,
} from "../utils/constants"

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    minWidth: "100vw",
    minHeight: "100vh",
    backgroundColor: oneTheme.folderLayout.background[theme.props.mode],
  },
  appBar: {
    backgroundColor: "transparent",
    padding: theme.spacing(1, 0),
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${SIDEBAR_WIDTH}px)`,
      marginLeft: SIDEBAR_WIDTH,
    },
  },
  appBarContent: {
    backgroundColor: oneTheme.folderLayout.searchBar[theme.props.mode],
    width: "100%",
    display: "flex",
    alignItems: "center",
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    border: 0,
    boxShadow: "0 4px 8px -8px rgba(0,0,0,0.4)",
    [theme.breakpoints.up("lg")]: {
      borderRadius: 10,
      paddingLeft: theme.spacing(2),
    },
  },
  input: {
    flex: 1,
    width: "100%",
  },
  toolbar: theme.mixins.toolbar,
  menuButton: {
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  searchButton: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  content: {
    flex: 1,
    flexGrow: 1,
    padding: theme.spacing(2, 2),
    overflow: "auto",
    [theme.breakpoints.up("sm")]: {
      padding: theme.spacing(2, 3),
    },
  },
  breadcrumbs: {
    marginBottom: theme.spacing(2),
  },
  searchBarDivider: {
    display: "none",
    height: 28,
    margin: 4,
    [theme.breakpoints.up("md")]: {
      display: "block",
    },
  },
  activeIcons: {
    color: "#0e2c4b",
  },
}))

const FolderLayout = ({ folderId, search }) => {
  const classes = useStyles()
  const searchRef = useRef()
  const { firebase, user } = useAuth()
  const { showNotification } = useNotification()
  const queryClient = useQueryClient()
  const location = useLocation()

  const [mobileOpen, setMobileOpen] = useState(false)
  const [showPath, setShowPath] = useState(false)
  const [showingSettings, setShowingSettings] = useState(false)
  const [showCreateDialog, setShowCreateDialog] = useState(false)
  const [showEditDialog, setShowEditDialog] = useState(false)

  const handleDrawerToggle = () => {
    setMobileOpen(prevValue => !prevValue)
  }

  const handlePathTraversedToggle = () => {
    setShowPath(prevValue => !prevValue)
  }

  const showSettings = () => {
    if (!showingSettings) {
      setShowingSettings(true)
    }
  }

  const hideSettings = () => {
    if (showingSettings) {
      setShowingSettings(false)
    }
  }

  const handleSearch = event => {
    event.preventDefault()
    let search = searchRef.current.value
    console.log("value", search)

    let nextPath = search.length
      ? `${location.pathname}?search=${searchRef.current.value}`
      : location.pathname
    navigate(nextPath)
  }

  const showCreateFolder = () => {
    setShowCreateDialog(true)
  }

  const hideCreateFolder = () => {
    setShowCreateDialog(false)
  }

  const handleCreateFolder = folderName => {
    createNewFolder.mutate(folderName)
  }

  const showEditFolder = () => {
    setShowEditDialog(true)
  }

  const hideEditFolder = () => {
    setShowEditDialog(false)
  }

  const handleEditFolder = newFolderName => {
    editFolder.mutate(newFolderName)
  }

  const handleDeleteFolder = () => {
    let currentFolder = data
    let nFiles = Object.keys(currentFolder.files).length
    let nFolders = Object.keys(currentFolder.folders).length

    if (nFiles > 0 || nFolders > 0) {
      showNotification({
        type: "error",
        message: "Folder must be empty to delete",
      })

      return
    }

    deleteFolder.mutate()
  }

  const handleUploadFile = event => {
    const files = event.target.files
    if (files.length > 0 && files[0]) {
      let file = files[0]
      console.log("FILE => ", file)

      // Check whether file's size is within limit
      if (file.size <= MAX_UPLOAD_FILE_SIZE) {
        // Check whether file's name is within limit
        if (String(file.name).length <= MAX_FNAME_LENGTH) {
          uploadFile.mutate(file)
        } else {
          showNotification({
            type: "warn",
            message: `File's name must be within ${MAX_FNAME_LENGTH} characters`,
          })
        }
      } else {
        showNotification({
          type: "warn",
          message: `Maximum file size can be ${simplifyFileSize(
            MAX_UPLOAD_FILE_SIZE
          )}`,
        })
      }
    }
  }

  const { data, status } = useQuery(
    [folderId, folderId],
    ({ queryKey }) => FolderAPI.getFolder(firebase, queryKey[1]),
    {
      cacheTime: Infinity,
      refetchOnWindowFocus: false,
      staleTime: 1000 * 1800,
      onError: error => {
        navigate("/404")
      },
    }
  )

  const createNewFolder = useMutation(
    async folderName => {
      let path = data.path.concat([{ id: data.id, name: data.name }])
      let folderObject = {
        name: folderName,
        path: path,
        owner: user.id,
        files: {},
        folders: {},
      }

      await FolderAPI.createFolder(firebase, folderObject, data.id)
    },
    {
      onMutate: () => {
        hideCreateFolder()
        console.log("creating folder...")
        showNotification({ type: "progress", message: `Creating new folder` })
      },
      onSuccess: (_, folderName) => {
        // refetch parent folder & show notification
        console.log("refetching parent folder...")
        queryClient.invalidateQueries(data.id)
        showNotification({ type: "success", message: `Created ${folderName}` })
      },
      onError: error => {
        console.error("ERROR at create folder => ", error)
        showNotification({ type: "error", message: "Failed to create folder" })
      },
    }
  )

  const editFolder = useMutation(
    async newFolderName => {
      let parentFolderId = data.path[data.path.length - 1].id

      await FolderAPI.renameFolder(
        firebase,
        data,
        newFolderName,
        parentFolderId
      )
    },
    {
      onMutate: () => {
        hideEditFolder()
        console.log("editing folder...")
        showNotification({ type: "progress", message: `Saving folder changes` })
      },
      onSuccess: () => {
        let parentFolderId = data.path[data.path.length - 1].id

        // refetch folder & parent folder and show notification
        console.log("refetching folder...")
        queryClient.invalidateQueries(data.id)
        queryClient.invalidateQueries(parentFolderId)
        showNotification({ type: "success", message: `Saved changes` })
      },
      onError: error => {
        console.error("ERROR at edit folder => ", error)
        showNotification({ type: "error", message: "Failed to save changes" })
      },
    }
  )

  // @helper
  const isPermanentDelete = data => {
    return data.path[0].id === user.trash
  }

  // Delete permanently if deleting from trash, otherwise move to trash
  const deleteFolder = useMutation(
    async () => {
      let parentFolderId = data.path[data.path.length - 1].id

      if (isPermanentDelete(data)) {
        await FolderAPI.permanentlyDeleteFolder(firebase, data, parentFolderId)
      } else {
        await FolderAPI.moveFolderToTrash(
          firebase,
          data,
          parentFolderId,
          user.trash
        )
      }
    },
    {
      onMutate: () => {
        console.log("deleting folder...")
        let deletedFolderName = data.name

        showNotification({
          type: "progress",
          message: `Deleting ${deletedFolderName}`,
        })
      },
      onSuccess: () => {
        let parentFolderId = data.path[data.path.length - 1].id
        let deletedFolderName = data.name

        // refetch parent folder & trash folder & current folder & show notification
        console.log("refetching folder...")

        if (isPermanentDelete(data)) {
          queryClient.invalidateQueries(parentFolderId)
          showNotification({
            type: "success",
            message: `Permanently deleted ${deletedFolderName}`,
          })
        } else {
          queryClient.invalidateQueries(data.id)
          queryClient.invalidateQueries(user.trash)
          queryClient.invalidateQueries(parentFolderId)
          showNotification({
            type: "success",
            message: `Moved ${deletedFolderName} to trash`,
          })
        }

        navigate(`/folder/${parentFolderId}`)
      },
      onError: error => {
        console.error("ERROR at delete folder => ", error)
        showNotification({ type: "error", message: "Failed to delete folder" })
      },
    }
  )

  const uploadFile = useMutation(
    async file => {
      console.log("FILE => ", file)

      let path = data.path.concat([{ id: data.id, name: data.name }])
      let fileObject = {
        name: file.name,
        path: path,
        owner: user.id,
        size: file.size,
        visibility: "private",
        sharableUntil: "",
        file: file,
      }
      console.log("fileObject => ", fileObject)

      let parentFolderId = data.id
      await FolderAPI.uploadFile(firebase, fileObject, parentFolderId)
    },
    {
      onMutate: file => {
        showNotification({
          type: "progress",
          message: `Uploading ${file.name}`,
        })
      },
      onSuccess: (_, file) => {
        // refetch folder & show notification
        console.log("refetching folder...")
        queryClient.invalidateQueries(data.id)
        showNotification({
          type: "success",
          message: `Uploaded ${file.name}`,
        })
      },
      onError: error => {
        console.error("ERROR at upload file => ", error)
        showNotification({ type: "error", message: "Failed to upload file" })
      },
    }
  )

  // early escape if firebase not found,
  // preventing build failure
  if (!firebase) {
    return null
  }

  return (
    <div className={classes.root}>
      <CssBaseline />

      <SideBar
        handleDrawerToggle={handleDrawerToggle}
        mobileOpen={mobileOpen}
        folderMetaStatus={status}
        folderMetaData={data}
        showSettings={showSettings}
        hideSettings={hideSettings}
        showCreateFolder={showCreateFolder}
        hideCreateFolder={hideCreateFolder}
        showEditFolder={showEditFolder}
        hideEditFolder={hideEditFolder}
        deleteFolder={handleDeleteFolder}
        uploadFile={handleUploadFile}
      />

      <AppBar
        position="fixed"
        elevation={0}
        className={classes.appBar}
        onSubmit={handleSearch}
      >
        <Toolbar>
          <Paper
            elevation={0}
            square={false}
            outlined="false"
            component="form"
            variant="outlined"
            className={classes.appBarContent}
          >
            <IconButton
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              className={clsx(classes.menuButton, classes.activeIcons)}
            >
              <MenuIcon />
            </IconButton>
            <IconButton
              aria-label="search icon"
              edge="start"
              disabled
              className={classes.searchButton}
            >
              <SearchIcon />
            </IconButton>
            <InputBase
              inputRef={searchRef}
              className={classes.input}
              placeholder="Search all files, folders"
              inputProps={{ "aria-label": "search all files, folders" }}
            />
            <Divider
              orientation="vertical"
              className={classes.searchBarDivider}
            />
            <IconButton
              color="primary"
              className={classes.iconButton}
              aria-label="directions"
              onClick={handlePathTraversedToggle}
            >
              {showPath ? (
                <ExpandLessIcon className={classes.activeIcons} />
              ) : (
                <ExpandMoreIcon className={classes.activeIcons} />
              )}
            </IconButton>
          </Paper>
        </Toolbar>
      </AppBar>

      <main className={classes.content}>
        <div className={classes.toolbar} />
        {showPath && (
          <Path className={classes.breadcrumbs} folderMetaData={data} />
        )}
        {showingSettings ? (
          <Settings hideSettings={hideSettings} />
        ) : search ? (
          <Search />
        ) : (
          <FolderContents folderMetaStatus={status} folderMetaData={data} />
        )}
      </main>

      {/* Create folder */}
      <FolderModal
        title={"Create new folder"}
        open={showCreateDialog}
        onClose={hideCreateFolder}
        onSubmit={handleCreateFolder}
        placeholder={"Folder name"}
        submitName={"Create"}
      />

      {/* Rename folder */}
      <FolderModal
        title={"Edit folder"}
        open={showEditDialog}
        onClose={hideEditFolder}
        onSubmit={handleEditFolder}
        placeholder={status === "success" && data.name}
        submitName={"Save"}
      />
    </div>
  )
}

export default FolderLayout
