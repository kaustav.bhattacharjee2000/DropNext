import React from "react"
import Typography from "@material-ui/core/Typography"
import Snackbar from "@material-ui/core/Snackbar"
import SnackbarContent from "@material-ui/core/SnackbarContent"
import ErrorIcon from "@material-ui/icons/Error"
import CloseIcon from "@material-ui/icons/Close"
import CircularProgress from "@material-ui/core/CircularProgress"
import CheckCircleIcon from "@material-ui/icons/CheckCircle"
import ReportProblemRoundedIcon from "@material-ui/icons/ReportProblemRounded"
import { makeStyles } from "@material-ui/core/styles"
import { green, red, yellow } from "@material-ui/core/colors"
import { nanoid } from "nanoid"

import { useNotification } from "../contexts/notification-context"

const useStyles = makeStyles(theme => ({
  alert: {
    width: "100%",
  },
  uploadingBody: {
    display: "flex",
    alignItems: "center",
    columnGap: 15,
  },
  snackbar: {
    margin: theme.spacing(1),
  },
  message: {
    display: "flex",
    alignItems: "center",
    columnGap: 15,
  },
}))

export default function Notification(props) {
  const classes = useStyles()
  const {
    notification,
    allowNotification,
    closeNotification,
    position,
  } = useNotification()

  const anchorOrigin = position || { vertical: "bottom", horizontal: "right" }

  const icons = {
    progress: {
      leftIcon: <CircularProgress size="1.5rem" style={{ color: "white" }} />,
    },
    success: {
      leftIcon: <CheckCircleIcon htmlColor={green[500]} />,
      rightIcon: (
        <CloseIcon onClick={closeNotification} style={{ cursor: "pointer" }} />
      ),
    },
    error: {
      leftIcon: <ErrorIcon htmlColor={red[400]} />,
      rightIcon: (
        <CloseIcon onClick={closeNotification} style={{ cursor: "pointer" }} />
      ),
    },
    warn: {
      leftIcon: <ReportProblemRoundedIcon htmlColor={yellow[500]} />,
      rightIcon: (
        <CloseIcon onClick={closeNotification} style={{ cursor: "pointer" }} />
      ),
    },
  }

  if (!notification) {
    return null
  }

  return (
    <Snackbar
      key={nanoid()}
      anchorOrigin={anchorOrigin}
      open={allowNotification}
      className={classes.snackbar}
    >
      <SnackbarContent
        action={icons[notification.type].rightIcon}
        message={
          <div className={classes.message}>
            {icons[notification.type].leftIcon}
            <Typography style={{ fontSize: "1rem" }}>
              {notification.message}
            </Typography>
          </div>
        }
      />
    </Snackbar>
  )
}
