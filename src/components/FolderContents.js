import React, { useState } from "react"
import { makeStyles } from "@material-ui/core/styles"
import Grid from "@material-ui/core/Grid"
import FolderIcon from "@material-ui/icons/Folder"
import InsertDriveFileIcon from "@material-ui/icons/InsertDriveFile"
import Typography from "@material-ui/core/Typography"
import { Link } from "gatsby"
import { nanoid } from "nanoid"

import FileInfo from "./FileInfo"
import Loading from "./Loading"
import { convertToFolderMeta } from "../utils/utils"
import oneTheme from "../utils/theme"

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2, 0),
  },
  itembox: {
    display: "flex",
    alignItems: "center",
    columnGap: theme.spacing(1),
    padding: theme.spacing(1.5, 2),
    backgroundColor: oneTheme.folderContents.itemBox[theme.props.mode],
    borderRadius: 10,
    boxShadow: "0 4px 20px -8px rgba(0,0,0,0.4)",
    cursor: "pointer",
  },
  caption: {
    fontWeight: "600",
    color: oneTheme.folderContents.caption[theme.props.mode],
    fontSize: 18,
  },
  text: {
    color: oneTheme.folderContents.text[theme.props.mode],
    fontSize: 16,
  },
  italics: {
    fontSize: 16,
    marginLeft: theme.spacing(1),
    color: oneTheme.folderContents.italics[theme.props.mode],
  },
  containerTitle: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: theme.spacing(1),
  },
  contentSeparator: {
    marginBottom: theme.spacing(2),
  },
  folderIcon: {
    color: "#01aeba",
  },
  fileIcon: {
    color: "#01c4d4",
  },
}))

function ItemBox({ folder, data }) {
  const classes = useStyles()

  // below methods are only used if it's a file
  const [fileInfoVisible, setFileInfoVisible] = useState(false)

  const handleFileInfoClose = () => {
    setFileInfoVisible(false)
  }

  const handleFileInfoOpen = () => {
    setFileInfoVisible(true)
  }

  const handleOnClick = () => {
    if (folder) {
      return
    }
    handleFileInfoOpen()
  }

  const icon = folder ? (
    <FolderIcon className={classes.folderIcon} />
  ) : (
    <InsertDriveFileIcon className={classes.fileIcon} />
  )

  const fileInfoDialogue = !folder && (
    <FileInfo
      open={fileInfoVisible}
      onClose={handleFileInfoClose}
      fileId={data.id}
    />
  )

  return (
    <React.Fragment>
      <div
        role="button"
        className={classes.itembox}
        tabIndex={0}
        onKeyDown={null}
        onClick={handleOnClick}
      >
        {icon}
        <Typography noWrap className={classes.text}>
          {data.name}
        </Typography>
      </div>
      {fileInfoDialogue}
    </React.Fragment>
  )
}

function DisplayGridItems({ title, children, emptyDataMessage }) {
  const classes = useStyles()

  return (
    <div className={classes.contentSeparator}>
      <div className={classes.containerTitle}>
        <Typography className={classes.caption}>{title}</Typography>
      </div>

      <Grid container spacing={2}>
        {children ? (
          React.Children.map(children, eachItem => (
            <Grid key={nanoid()} item xs={6} lg={3}>
              {eachItem}
            </Grid>
          ))
        ) : (
          <Grid key={nanoid()}>
            <Typography className={classes.italics}>
              {emptyDataMessage}
            </Typography>
          </Grid>
        )}
      </Grid>
    </div>
  )
}

function FolderContents(props) {
  const classes = useStyles()

  if (props.folderMetaStatus === "loading") {
    return <Loading margin={2} thickness={2} size="5rem" />
  } else if (
    props.folderMetaStatus === "error" ||
    !props.folderMetaData ||
    convertToFolderMeta(props.folderMetaData).length === 0
  ) {
    return null
  }

  const foldersData = convertToFolderMeta(props.folderMetaData.folders)
  const filesData = convertToFolderMeta(props.folderMetaData.files)

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item key={nanoid()} xs={12}>
          <DisplayGridItems
            key={nanoid()}
            title={"Folders"}
            emptyDataMessage={"No folders to show"}
          >
            {foldersData.length > 0 &&
              foldersData.map(({ id, name }) => (
                <Link
                  key={id}
                  style={{ textDecoration: "inherit" }}
                  to={`/folder/${id}`}
                >
                  <ItemBox data={{ id, name }} folder />
                </Link>
              ))}
          </DisplayGridItems>
        </Grid>

        <Grid item key={nanoid()} xs={12}>
          <DisplayGridItems
            key={nanoid()}
            title={"Files"}
            emptyDataMessage={"No files to show"}
          >
            {filesData.length > 0 &&
              filesData.map(({ id, name }) => (
                <ItemBox key={id} data={{ id, name }} />
              ))}
          </DisplayGridItems>
        </Grid>
      </Grid>
    </div>
  )
}

export default FolderContents
