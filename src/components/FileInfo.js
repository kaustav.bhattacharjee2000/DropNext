import React, { Fragment, useState } from "react"
import Drawer from "@material-ui/core/Drawer"
import Grid from "@material-ui/core/Grid"
import EditIcon from "@material-ui/icons/Edit"
import DeleteIcon from "@material-ui/icons/Delete"
import InsertDriveFileIcon from "@material-ui/icons/InsertDriveFile"
import OpenInNewIcon from "@material-ui/icons/OpenInNew"
import CloseRoundedIcon from "@material-ui/icons/CloseRounded"
import ReplyIcon from "@material-ui/icons/Reply"
import Divider from "@material-ui/core/Divider"
import Typography from "@material-ui/core/Typography"
import Dialog from "@material-ui/core/Dialog"
import LinkIcon from "@material-ui/icons/Link"
import { makeStyles } from "@material-ui/core/styles"
import Box from "@material-ui/core/Box"
import { useQuery, useMutation, useQueryClient } from "react-query"
import clsx from "clsx"

import Loading from "./Loading"
import CustomButton from "./CustomButton"
import DatePicker from "./DatePicker"
import * as FolderAPI from "../api/folder"
import { useAuth } from "../contexts/auth-context"
import { useNotification } from "../contexts/notification-context"
import oneTheme from "../utils/theme"
import { simplifyFileSize, dayStart, stripToken } from "../utils/utils"
import { FILE_MODAL_WIDTH, MAX_UPLOAD_FILE_SIZE } from "../utils/constants"

const useStyles = makeStyles(theme => ({
  mobileDevices: {
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  otherDevices: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  transparentBg: {
    backgroundColor: "transparent",
  },
  screen: {
    backgroundColor: oneTheme.fileInfo.background[theme.props.mode],
    color: oneTheme.fileInfo.text[theme.props.mode],
    [theme.breakpoints.down("sm")]: {
      width: "100vw",
      height: "auto",
      borderTopLeftRadius: theme.spacing(3),
      borderTopRightRadius: theme.spacing(3),
      padding: theme.spacing(4),
    },
    [theme.breakpoints.up("sm")]: {
      maxWidth: FILE_MODAL_WIDTH,
      padding: theme.spacing(4),
      borderRadius: theme.spacing(2),
    },
  },
  text: {
    fontSize: "1rem",
    color: "inherit",
  },
  customizeButtonTitle: {
    fontSize: 14,
  },
  customizeButtonIcon: {
    height: "1.5rem",
    width: "1.5rem",
    [theme.breakpoints.up("sm")]: {
      height: "1rem",
      width: "1rem",
    },
  },
  drawerTitle: {
    fontSize: "1.1rem",
    fontWeight: "bolder",
  },
  fileIcon: {
    color: "#01aeba",
    height: "1.75rem",
    width: "1.75rem",
    marginRight: 16,
  },
  divider: {
    margin: theme.spacing(2, 0),
    height: theme.spacing(0.25),
    backgroundColor: oneTheme.fileInfo.divider[theme.props.mode],
  },
  visibilityButton: {
    borderRadius: 3,
    backgroundColor: "rgba(0,0,0,0.1)",
    color: oneTheme.fileInfo.visibilityButtonColor[theme.props.mode],
    fontSize: 14,
    textTransform: "none",
    padding: theme.spacing(0.75, 2),
    cursor: "pointer",
  },
  selected: {
    backgroundColor: "#017a82",
    color: "white",
  },
  toggleButtonGroup: {
    display: "flex",
  },
  disabledButton: {
    color: oneTheme.sideBar.disabledButtonText[theme.props.mode],
    border: oneTheme.sideBar.disabledButtonBorder[theme.props.mode],
    "&:hover": {
      color: oneTheme.sideBar.disabledButtonText[theme.props.mode],
      border: oneTheme.sideBar.disabledButtonBorder[theme.props.mode],
    },
  },
  textBoxContainer: {
    flexGrow: 1,
  },
  textBoxInput: {
    width: "100%",
    boxSizing: "border-box",
    backgroundColor: "transparent",
    padding: theme.spacing(0.75, 1),
    marginTop: theme.spacing(1),
    outline: "none",
    borderRadius: 3,
    border: oneTheme.joinPage.inputBoxBorder[theme.props.mode],
    borderWidth: 1,
    fontSize: 16,
    fontFamily: "Nunito Sans",
    color: oneTheme.joinPage.text[theme.props.mode],
  },
  link: {
    color: "#01aeba",
    cursor: "pointer",
    transform: "rotate(-45deg)",
  },
  datePicker: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    padding: theme.spacing(0, 2),
    [theme.breakpoints.up("sm")]: {
      justifyContent: "flex-end",
    },
  },
}))

function FileDetails(props) {
  const classes = useStyles()
  const { user } = useAuth()
  const { showNotification } = useNotification()
  const [name, setName] = useState(props.fileMetaData.name)
  const [visibility, setVisibility] = useState(props.fileMetaData.visibility)

  // "sharableUntil" is in unix time, if its -1 it means that
  // it will always be public, otherwise a valid unix time
  // will be set
  const [sharableUntil, setSharableUntil] = useState(
    props.fileMetaData.sharableUntil
  )

  const handleVisibilityChange = newVisibility => {
    // change only if "editing" is set in parent
    if (props.editing) {
      setVisibility(newVisibility)
    }
  }

  const handleNameChange = event => {
    let newName = event.target.value
    console.log(newName)

    // change only if "editing" is set in parent
    if (props.editing) {
      setName(newName)
    }
  }

  const saveSharableUntil = day => {
    setSharableUntil(dayStart(day, true))
  }

  const getSharableUntil = () => {
    if (sharableUntil === -1) {
      return "Always"
    }
    return new Date(sharableUntil).toLocaleDateString()
  }

  const handleSave = () => {
    // This object contains the fields needed to be updated
    let updateFileMeta = {
      sharableUntil,
    }

    // Additional data contains file meta and owner's info
    let additionalData = {
      file: props.fileMetaData,
      owner: user,
    }

    console.log("additionData => ", additionalData)

    if (visibility !== props.fileMetaData.visibility) {
      updateFileMeta.visibility = visibility
    }

    if (name && name.length > 0 && name !== props.fileMetaData.name) {
      updateFileMeta.name = name
    }

    console.log("updateFileMeta => ", updateFileMeta)
    props.handleSaveFile(updateFileMeta, additionalData)
  }

  const cancel = () => {
    setSharableUntil(props.fileMetaData.sharableUntil)
    setVisibility(props.fileMetaData.visibility)
    setName(props.fileMetaData.name)
    props.handleStopEditing()
  }

  const handleCopyLink = () => {
    if (typeof window && window && window.navigator) {
      let copyUrl = `${window.location.origin}/share/${props.fileMetaData.shortLink}`
      navigator.clipboard.writeText(copyUrl)
      showNotification({ type: "success", message: "Link copied to clipboard" })
    }
  }

  const handleReplaceFile = event => {
    const files = event.target.files

    if (files.length > 0 && files[0]) {
      let file = files[0]
      console.log("FILE => ", file)

      // Check whether file's size is within limit
      if (file.size <= MAX_UPLOAD_FILE_SIZE) {
        props.handleReplaceFile(file)
      } else {
        showNotification({
          type: "warn",
          message: `Maximum file size can be ${simplifyFileSize(
            MAX_UPLOAD_FILE_SIZE
          )}`,
        })
      }
    }
  }

  // Don't allow to edit file if it's in trash
  const allowFileEditing =
    props.fileMetaData && props.fileMetaData.path[0].id !== user.trash

  // Conditional rendering of components if not loaded yet
  if (props.fileMetaStatus === "loading") {
    return <Loading margin={2} size="2rem" />
  } else if (
    props.fileMetaStatus === "error" ||
    !props.fileMetaData ||
    Object.keys(props.fileMetaData).length === 0
  ) {
    return null
  }

  return (
    <div className={classes.screen} role="presentation">
      {/* File icon, file name and short link icon */}

      <Box display="flex" alignItems="center">
        <InsertDriveFileIcon className={classes.fileIcon} />
        <Box className={classes.textBoxContainer}>
          {props.editing ? (
            <input
              name="name"
              className={classes.textBoxInput}
              placeholder={props.fileMetaData.name}
              onChange={handleNameChange}
            />
          ) : (
            <Typography className={classes.drawerTitle}>
              {props.fileMetaData.name}
            </Typography>
          )}
        </Box>
        {props.fileMetaData.visibility === "public" && (
          <LinkIcon className={classes.link} onClick={handleCopyLink} />
        )}
      </Box>

      <Divider className={classes.divider} />

      {/* File meta data */}

      <Grid container spacing={4}>
        <Grid item container spacing={1}>
          <Grid container item xs={12} justify="space-between">
            <Grid item className={classes.text}>
              Size
            </Grid>
            <Grid item className={classes.text}>
              {simplifyFileSize(props.fileMetaData.size)}
            </Grid>
          </Grid>

          <Grid container item xs={12} justify="space-between">
            <Grid item className={classes.text}>
              Created on
            </Grid>
            <Grid item className={classes.text}>
              {props.fileMetaData.createdAt.toDate().toLocaleDateString()}
            </Grid>
          </Grid>

          <Grid container item xs={12} justify="space-between">
            <Grid item className={classes.text}>
              Modified on
            </Grid>
            <Grid item className={classes.text}>
              {props.fileMetaData.modifiedAt.toDate().toLocaleDateString()}
            </Grid>
          </Grid>

          <Grid
            container
            item
            xs={12}
            justify="space-between"
            alignItems="center"
          >
            <Grid item className={classes.text}>
              Visibility
            </Grid>
            <Grid item className={classes.text}>
              <Box className={classes.toggleButtonGroup}>
                <Box
                  className={clsx(
                    classes.visibilityButton,
                    visibility === "private" && classes.selected
                  )}
                  onClick={() => handleVisibilityChange("private")}
                >
                  Private
                </Box>
                <Box
                  className={clsx(
                    classes.visibilityButton,
                    visibility === "public" && classes.selected
                  )}
                  onClick={() => handleVisibilityChange("public")}
                >
                  Public
                </Box>
              </Box>
            </Grid>
          </Grid>

          {visibility === "public" && (
            <Grid
              container
              item
              xs={12}
              justify="space-between"
              alignItems="center"
            >
              <Grid item className={classes.text}>
                Sharable until
              </Grid>
              <Grid item className={classes.text}>
                {!props.editing ? (
                  <Typography>{getSharableUntil()}</Typography>
                ) : (
                  <Box className={classes.toggleButtonGroup}>
                    <Box
                      className={clsx(
                        classes.visibilityButton,
                        getSharableUntil() === "Always" && classes.selected
                      )}
                      onClick={() => setSharableUntil(-1)}
                    >
                      Always
                    </Box>
                    <Box
                      className={clsx(
                        classes.visibilityButton,
                        getSharableUntil() !== "Always" && classes.selected
                      )}
                      onClick={() => setSharableUntil(dayStart(Date(), true))}
                    >
                      Fixed
                    </Box>
                  </Box>
                )}
              </Grid>
            </Grid>
          )}
        </Grid>

        {props.editing &&
          visibility === "public" &&
          getSharableUntil() !== "Always" && (
            <Box className={classes.datePicker}>
              <DatePicker
                day={new Date(sharableUntil)}
                saveSharableUntil={saveSharableUntil}
              />
            </Box>
          )}

        {/* File action buttons */}

        <Grid item container spacing={2}>
          <Grid item xs={6} sm={3}>
            <CustomButton
              title={props.editing ? "Save" : "Edit"}
              icon={EditIcon}
              titleStyle={classes.customizeButtonTitle}
              iconStyle={classes.customizeButtonIcon}
              onClick={
                allowFileEditing
                  ? props.editing
                    ? handleSave
                    : props.handleStartEditing
                  : null
              }
              containerStyle={!allowFileEditing && classes.disabledButton}
            />
          </Grid>

          <Grid item xs={6} sm={3}>
            {!props.editing ? (
              <a
                href={stripToken(props.fileMetaData.downloadUrl)}
                target="_blank"
                rel="noreferrer"
                style={{ textDecoration: "none" }}
              >
                <CustomButton
                  title={"Open"}
                  icon={OpenInNewIcon}
                  titleStyle={classes.customizeButtonTitle}
                  iconStyle={classes.customizeButtonIcon}
                />
              </a>
            ) : (
              <CustomButton
                title={"Cancel"}
                icon={CloseRoundedIcon}
                titleStyle={classes.customizeButtonTitle}
                iconStyle={classes.customizeButtonIcon}
                onClick={cancel}
              />
            )}
          </Grid>

          <Grid item xs={6} sm={3}>
            <CustomButton
              title={"Delete"}
              icon={DeleteIcon}
              titleStyle={classes.customizeButtonTitle}
              iconStyle={classes.customizeButtonIcon}
              onClick={!props.editing ? props.handleDeleteFile : null}
              containerStyle={props.editing && classes.disabledButton}
            />
          </Grid>

          <Grid item xs={6} sm={3}>
            <label htmlFor="replace-file">
              <CustomButton
                title={"Replace"}
                icon={ReplyIcon}
                titleStyle={classes.customizeButtonTitle}
                iconStyle={classes.customizeButtonIcon}
                containerStyle={
                  (!allowFileEditing || props.editing) && classes.disabledButton
                }
              />
            </label>
            <input
              type="file"
              id="replace-file"
              onClick={e => {
                if (!allowFileEditing || props.editing) {
                  e.preventDefault()
                }
                e.target.value = ""
              }}
              onChange={
                !allowFileEditing || props.editing ? null : handleReplaceFile
              }
              style={{ display: "none" }}
            />
          </Grid>
        </Grid>
      </Grid>
    </div>
  )
}

function FileInfo({ open, onClose, fileId }) {
  const classes = useStyles()
  const { user, firebase } = useAuth()
  const { showNotification } = useNotification()
  const queryClient = useQueryClient()
  const [editing, setEditing] = useState(false)

  const { data, status } = useQuery(
    [fileId, fileId],
    ({ queryKey }) => FolderAPI.getFile(firebase, queryKey[1]),
    {
      cacheTime: Infinity,
      refetchOnWindowFocus: false,
      staleTime: 1000 * 1800,
      onSuccess: () => {
        console.log("Got file !")
      },
      onError: error => {
        console.error("File fetching error ! => ", error)
        onClose()
        showNotification({ type: "error", message: "Cannot retrieve file" })
      },
    }
  )

  const handleStartEditing = () => {
    setEditing(true)
  }

  const handleStopEditing = () => {
    setEditing(false)
  }

  const handleSaveFile = (updateFileMeta, additionalData) => {
    if (Object.keys(updateFileMeta).length > 0) {
      editFile.mutate({ updateFileMeta, additionalData })
    }
    handleStopEditing()
  }

  const handleDeleteFile = () => {
    deleteFile.mutate()
  }

  const handleReplaceFile = newFile => {
    let fileObject = { file: newFile, ...data }
    replaceFile.mutate(fileObject)
  }

  const editFile = useMutation(
    async ({ updateFileMeta, additionalData }) => {
      let parentFolderId = data.path[data.path.length - 1].id

      console.log("NEW updateFileMeta => ", updateFileMeta)
      console.log("NEW additionalData => ", additionalData)

      await FolderAPI.updateFile(
        firebase,
        data.id,
        updateFileMeta,
        additionalData,
        parentFolderId
      )
    },
    {
      onMutate: () => {
        console.log("editing file...")
        showNotification({ type: "progress", message: `Saving file changes` })
      },
      onSuccess: () => {
        let parentFolderId = data.path[data.path.length - 1].id

        // refetch folder & parent folder and show notification
        console.log("refetching folder...")
        queryClient.invalidateQueries(data.id)
        queryClient.invalidateQueries(parentFolderId)
        showNotification({ type: "success", message: `Saved changes` })
      },
      onError: error => {
        console.error("ERROR at edit folder => ", error)
        showNotification({ type: "error", message: "Failed to save changes" })
      },
    }
  )

  // @helper
  const isPermanentDelete = data => {
    return data.path[0].id === user.trash
  }

  // Delete permanently if deleting from trash, otherwise move to trash
  const deleteFile = useMutation(
    async () => {
      let parentFolderId = data.path[data.path.length - 1].id

      console.log("Call to delete this file object ===> ", data)

      if (isPermanentDelete(data)) {
        await FolderAPI.permanentlyDeleteFile(
          firebase,
          data,
          data.storagePath,
          parentFolderId
        )
      } else {
        await FolderAPI.moveFileToTrash(
          firebase,
          data,
          parentFolderId,
          user.trash
        )
      }
    },
    {
      onMutate: () => {
        console.log("deleting file...")
        let deletedFileName = data.name

        showNotification({
          type: "progress",
          message: `Deleting ${deletedFileName}`,
        })
      },
      onSuccess: () => {
        let parentFolderId = data.path[data.path.length - 1].id
        let deletedFileName = data.name

        // refetch parent folder & trash folder & deleted file & show notification
        console.log("refetching parent folder...")

        if (isPermanentDelete(data)) {
          queryClient.invalidateQueries(parentFolderId)

          showNotification({
            type: "success",
            message: `Permanently deleted ${deletedFileName}`,
          })
        } else {
          queryClient.invalidateQueries(user.trash)
          queryClient.invalidateQueries(parentFolderId)
          queryClient.invalidateQueries(data.id)

          showNotification({
            type: "success",
            message: `Moved ${deletedFileName} to trash`,
          })
        }

        onClose()
      },
      onError: error => {
        console.error("ERROR at delete file => ", error)
        showNotification({ type: "error", message: "Failed to delete file" })
      },
    }
  )

  const replaceFile = useMutation(
    async fileObject => {
      await FolderAPI.replaceFile(firebase, fileObject, data.storagePath)
    },
    {
      onMutate: () => {
        console.log("replacing file...")

        showNotification({
          type: "progress",
          message: `Replacing ${data.name}`,
        })
      },
      onSuccess: () => {
        queryClient.invalidateQueries(data.id)
        showNotification({
          type: "success",
          message: `Replaced ${data.name}`,
        })
      },
      onError: error => {
        console.error("ERROR at replace file => ", error)
        showNotification({ type: "error", message: "Failed to replace file" })
      },
    }
  )

  return (
    <Fragment>
      <Drawer
        anchor={"bottom"}
        open={open}
        onClose={onClose}
        PaperProps={{
          elevation: 0,
          className: classes.transparentBg,
        }}
        className={classes.mobileDevices}
      >
        <FileDetails
          fileMetaStatus={status}
          fileMetaData={data}
          handleDeleteFile={handleDeleteFile}
          handleStartEditing={handleStartEditing}
          handleStopEditing={handleStopEditing}
          handleSaveFile={handleSaveFile}
          handleReplaceFile={handleReplaceFile}
          editing={editing}
        />
      </Drawer>

      <Dialog
        open={open}
        onClose={onClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        className={classes.otherDevices}
        PaperProps={{
          className: classes.transparentBg,
        }}
      >
        <FileDetails
          fileMetaStatus={status}
          fileMetaData={data}
          handleDeleteFile={handleDeleteFile}
          handleStartEditing={handleStartEditing}
          handleStopEditing={handleStopEditing}
          handleSaveFile={handleSaveFile}
          handleReplaceFile={handleReplaceFile}
          editing={editing}
        />
      </Dialog>
    </Fragment>
  )
}

export default FileInfo
