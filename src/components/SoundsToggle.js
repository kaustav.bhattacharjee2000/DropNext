import React from "react"
import VolumeUpIcon from "@material-ui/icons/VolumeUp"
import VolumeOffIcon from "@material-ui/icons/VolumeOff"

import { useAuth } from "../contexts/auth-context"

function SoundsToggle(props) {
  const { settings, updateSettings } = useAuth()

  const toggleSounds = () => {
    updateSettings({ notificationSounds: !settings.notificationSounds })
  }

  return settings.notificationSounds ? (
    <VolumeUpIcon onClick={toggleSounds} />
  ) : (
    <VolumeOffIcon onClick={toggleSounds} />
  )
}

export default SoundsToggle
