import React from "react"
import Typography from "@material-ui/core/Typography"
import { makeStyles } from "@material-ui/core/styles"
import clsx from "clsx"

import oneTheme from "../utils/theme"

const useStyles = makeStyles(theme => ({
  footerActionButton: {
    boxSizing: "border-box",
    width: "100%",
    display: "flex",
    alignItems: "center",
    columnGap: theme.spacing(1),
    border: oneTheme.customButton.defaultBorder[theme.props.mode],
    color: oneTheme.customButton.defaultText[theme.props.mode],
    borderRadius: 5,
    padding: theme.spacing(0.75, 0.5),
    cursor: "pointer",
    "&:hover": {
      color: oneTheme.customButton.hoverText[theme.props.mode],
      border: oneTheme.customButton.hoverBorder[theme.props.mode],
    },
  },
  footerActionIcon: {
    height: 20,
    width: 20,
  },
  title: {
    color: "inherit",
    fontSize: 15,
  },
}))

function CustomButton(props) {
  const {
    title,
    containerStyle,
    titleStyle,
    iconStyle,
    icon: Icon,
    children,
    ...restProps
  } = props
  const classes = useStyles()

  const wrapperClassName = clsx(
    classes.footerActionButton,
    containerStyle || {}
  )

  const titleClassName = clsx(classes.title, titleStyle || {})

  const iconClassName = clsx(classes.footerActionIcon, iconStyle || {})

  return (
    <div role="button" tabIndex={0} className={wrapperClassName} {...restProps}>
      <Icon className={iconClassName} />
      <Typography noWrap={true} className={titleClassName}>
        {title}
      </Typography>
      {children}
    </div>
  )
}

export default CustomButton
