import React, { useRef } from "react"
import { makeStyles } from "@material-ui/core/styles"
import Typography from "@material-ui/core/Typography"
import Dialog from "@material-ui/core/Dialog"
import Box from "@material-ui/core/Box"

import oneTheme from "../utils/theme"
import { FOLDER_MODAL_WIDTH, MAX_FNAME_LENGTH } from "../utils/constants"

const useStyles = makeStyles(theme => ({
  fullScreen: {
    padding: theme.spacing(2),
  },
  overlay: {
    margin: 0,
    backgroundColor: "transparent",
    width: "100%",
    boxShadow: "none",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  screen: {
    backgroundColor: "#373740",
    color: "white",
    borderRadius: theme.spacing(1),
    padding: theme.spacing(2),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: FOLDER_MODAL_WIDTH,
      padding: theme.spacing(4),
    },
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
  },
  textBoxInput: {
    margin: theme.spacing(3, 0),

    width: "100%",
    boxSizing: "border-box",
    backgroundColor: "transparent",
    padding: theme.spacing(1.25, 2),
    outline: "none",
    borderRadius: 3,
    border: oneTheme.joinPage.inputBoxBorder[theme.props.mode],
    borderWidth: 1,
    fontSize: 16,
    fontFamily: "Nunito Sans",
    color: oneTheme.joinPage.text[theme.props.mode],
  },
  actions: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    columnGap: theme.spacing(4),
  },
  button: {
    width: "100%",
    padding: theme.spacing(1.25),
    borderRadius: 3,
    textAlign: "center",
    color: "inherit",
    border: "1px solid rgba(255,255,255,0.2)",
    cursor: "pointer",
    "&:hover": {
      border: "1px solid rgba(255,255,255,0.6)",
    },
  },
}))

function FolderModal({
  title,
  placeholder,
  submitName,
  open,
  onClose,
  onSubmit,
}) {
  const classes = useStyles()
  const inputRef = useRef()

  const handleSubmit = () => {
    let name = inputRef.current.value
    console.log("typed => ", name)

    if (name && name.length > 0) {
      onSubmit(name)
    }
  }

  // Make sure name length is within limits
  const onChangeText = event => {
    if (inputRef.current.value.length > MAX_FNAME_LENGTH) {
      inputRef.current.value = inputRef.current.value.substring(
        0,
        MAX_FNAME_LENGTH
      )
    }
  }

  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      className={classes.fullScreen}
      PaperProps={{
        className: classes.overlay,
      }}
    >
      <Box className={classes.screen}>
        <Typography className={classes.title}>{title}</Typography>
        <input
          ref={inputRef}
          className={classes.textBoxInput}
          placeholder={placeholder}
          name="name"
          onChange={onChangeText}
          autoFocus
        />
        <Box className={classes.actions}>
          <Box className={classes.button} onClick={onClose}>
            Cancel
          </Box>
          <Box className={classes.button} onClick={handleSubmit}>
            {submitName}
          </Box>
        </Box>
      </Box>
    </Dialog>
  )
}

export default FolderModal
