import React from "react"
import { Link } from "gatsby"
import Drawer from "@material-ui/core/Drawer"
import Hidden from "@material-ui/core/Hidden"
import Divider from "@material-ui/core/Divider"
import Grid from "@material-ui/core/Grid"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemIcon from "@material-ui/core/ListItemIcon"
import ListItemText from "@material-ui/core/ListItemText"
import FolderIcon from "@material-ui/icons/Folder"
import DeleteIcon from "@material-ui/icons/Delete"
import AddIcon from "@material-ui/icons/Add"
import EditIcon from "@material-ui/icons/Edit"
import SettingsIcon from "@material-ui/icons/Settings"
import PublishIcon from "@material-ui/icons/Publish"
import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew"
import Typography from "@material-ui/core/Typography"
import { makeStyles } from "@material-ui/core/styles"

import AppName from "./AppName"
import CustomButton from "./CustomButton"
import Loading from "./Loading"
import { useAuth } from "../contexts/auth-context"
import { getObjectKeysLength } from "../utils/utils"
import oneTheme from "../utils/theme"
import {
  SIDEBAR_WIDTH,
  MAX_NESTED_FOLDERS_DEPTH,
  MAX_CHILD_FOLDERS,
  MAX_CHILD_FILES,
} from "../utils/constants"

const useStyles = makeStyles(theme => ({
  root: {
    height: "100%",
    backgroundColor: oneTheme.sideBar.background[theme.props.mode],
    display: "flex",
    flexDirection: "column",
  },
  drawer: {
    borderRight: 0,
    minHeight: "100vh",
    [theme.breakpoints.up("sm")]: {
      width: SIDEBAR_WIDTH,
      flexShrink: 0,
    },
  },
  drawerPaper: {
    width: SIDEBAR_WIDTH,
    borderRight: 0,
  },
  driveOption: {
    color: oneTheme.sideBar.tabs[theme.props.mode],
    "&:hover": {
      color: "#03b7c2",
    },
  },
  defaultText: {
    color: "inherit",
  },
  icon: {
    color: "inherit",
  },
  appname: {
    width: "100%",
    height: "auto",
    padding: theme.spacing(3, 8, 0, 2),
  },
  divider: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    backgroundColor: oneTheme.sideBar.separator[theme.props.mode],
  },
  footer: {
    padding: theme.spacing(2, 2, 2, 2),
    flexGrow: 1,
    display: "flex",
    flexFlow: "column",
  },
  folderDetailsTitle: {
    fontWeight: "bolder",
    color: oneTheme.sideBar.detailsTitle[theme.props.mode],
  },
  folderDetails: {
    color: oneTheme.sideBar.detailsContent[theme.props.mode],
  },
  listItemIcon: {
    minWidth: "auto",
    marginRight: theme.spacing(1),
  },
  disabledButton: {
    color: oneTheme.sideBar.disabledButtonText[theme.props.mode],
    border: oneTheme.sideBar.disabledButtonBorder[theme.props.mode],
    "&:hover": {
      color: oneTheme.sideBar.disabledButtonText[theme.props.mode],
      border: oneTheme.sideBar.disabledButtonBorder[theme.props.mode],
    },
  },
}))

function FolderDetails(props) {
  const classes = useStyles()
  const { user } = useAuth()

  if (props.folderMetaStatus === "loading") {
    return <Loading margin={2} size="2rem" />
  } else if (
    props.folderMetaStatus === "error" ||
    !props.folderMetaData ||
    getObjectKeysLength(props.folderMetaData) === 0
  ) {
    return null
  }

  // Operations like edit/delete cannot be performed
  // for root folders - Drive, Trash.
  const rootFolders = [user.drive, user.trash]
  const isRootFolder = rootFolders.includes(props.folderMetaData.id)
  const isInsideTrash =
    props.folderMetaData.id === user.trash ||
    (props.folderMetaData.path.length > 0 &&
      props.folderMetaData.path[0].id === user.trash)

  // Check whether any more folder can be created.
  // (+1) is used in 2nd condition because current
  // folder is not included in it's path.
  const allowCreateFolder =
    getObjectKeysLength(props.folderMetaData.folders) < MAX_CHILD_FOLDERS &&
    props.folderMetaData.path.length + 1 < MAX_NESTED_FOLDERS_DEPTH &&
    !isInsideTrash

  // Check whether any more file can be uploaded.
  const allowUploadFile =
    getObjectKeysLength(props.folderMetaData.files) < MAX_CHILD_FILES &&
    !isInsideTrash

  // Check if folder can be renamed
  const allowRenameFolder = !isRootFolder && !isInsideTrash

  // Check if folder can be deleted
  const allowDeleteFolder = !isRootFolder

  return (
    <Grid
      key={props.folderMetaData.id}
      container
      spacing={3}
      className={classes.folderDetails}
    >
      <Grid item xs={12}>
        {/* Current folder's name with folder icon */}
        <Grid container spacing={1} style={{ marginTop: 8 }}>
          <Grid item>
            <FolderIcon style={{ color: "#01aeba" }} />
          </Grid>
          <Grid item>
            <Typography
              className={classes.defaultText}
              style={{ fontWeight: "light" }}
            >
              {props.folderMetaData.name}
            </Typography>
          </Grid>
        </Grid>

        {/* Current folder's details */}
        <Grid container spacing={1} style={{ fontSize: 15 }}>
          <Grid container item xs={12} justify="space-between">
            <Grid item className={classes.defaultText}>
              Files
            </Grid>
            <Grid item className={classes.defaultText}>
              {getObjectKeysLength(props.folderMetaData.files)}
            </Grid>
          </Grid>

          <Grid container item xs={12} justify="space-between">
            <Grid item className={classes.defaultText}>
              Folders
            </Grid>
            <Grid item className={classes.defaultText}>
              {getObjectKeysLength(props.folderMetaData.folders)}
            </Grid>
          </Grid>

          <Grid container item xs={12} justify="space-between">
            <Grid item className={classes.defaultText}>
              Created on
            </Grid>
            <Grid item className={classes.defaultText}>
              {props.folderMetaData.createdAt.toDate().toLocaleDateString()}
            </Grid>
          </Grid>

          <Grid container item xs={12} justify="space-between">
            <Grid item className={classes.defaultText}>
              Modified on
            </Grid>
            <Grid item className={classes.defaultText}>
              {props.folderMetaData.modifiedAt.toDate().toLocaleDateString()}
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      {/* justify="flex-end" */}
      <Grid item container direction="column" xs={12}>
        {/* Footer actions */}
        <Grid container justify="space-between">
          <Grid item container xs={6} direction="column" spacing={1}>
            <Grid item>
              <label htmlFor="upload-file">
                <CustomButton
                  title={"File"}
                  icon={PublishIcon}
                  containerStyle={!allowUploadFile && classes.disabledButton}
                />
              </label>
              <input
                type="file"
                id="upload-file"
                onClick={e => {
                  if (!allowUploadFile) {
                    e.preventDefault()
                  }
                  e.target.value = ""
                }}
                onChange={!allowUploadFile ? null : props.uploadFile}
                style={{ display: "none" }}
              />
            </Grid>

            <Grid item>
              <CustomButton
                title={"Edit"}
                icon={EditIcon}
                onClick={!allowRenameFolder ? null : props.showEditFolder}
                containerStyle={!allowRenameFolder && classes.disabledButton}
              />
            </Grid>
          </Grid>

          <Grid item container xs={6} direction="column" spacing={1}>
            <Grid item>
              <CustomButton
                title={"Folder"}
                icon={AddIcon}
                onClick={!allowCreateFolder ? null : props.showCreateFolder}
                containerStyle={!allowCreateFolder && classes.disabledButton}
              />
            </Grid>

            <Grid item>
              <CustomButton
                title={"Delete"}
                icon={DeleteIcon}
                onClick={!allowDeleteFolder ? null : props.deleteFolder}
                containerStyle={!allowDeleteFolder && classes.disabledButton}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

function SideBar(props) {
  const classes = useStyles()
  const { user, logout } = useAuth()

  const listItems = [
    {
      icon: FolderIcon,
      title: "Drive",
      link: user && user.drive && `/folder/${user.drive}`,
      onClick: props.hideSettings,
    },
    {
      icon: DeleteIcon,
      title: "Trash",
      link: user && user.trash && `/folder/${user.trash}`,
      onClick: props.hideSettings,
    },
    { icon: SettingsIcon, title: "Settings", onClick: props.showSettings },
    { icon: PowerSettingsNewIcon, title: "Logout", onClick: logout },
  ]

  const renderDrawerItem = ({ title, icon: Icon, link, onClick }) => {
    const BaseItem = () => (
      <ListItem button className={classes.driveOption} onClick={onClick}>
        <ListItemIcon
          className={classes.icon}
          classes={{ root: classes.listItemIcon }}
        >
          <Icon />
        </ListItemIcon>
        <ListItemText primary={title} className={classes.defaultText} />
      </ListItem>
    )

    if (link) {
      return (
        <Link
          key={`sidebar_${title}`}
          style={{ textDecoration: "inherit" }}
          to={link}
        >
          <BaseItem key={title} />
        </Link>
      )
    }

    return <BaseItem key={title} />
  }

  const drawer = (
    <div className={classes.root}>
      <span>
        <Link to="/">
          <AppName fill="#01c4d4" className={classes.appname} />
        </Link>
        <List>{listItems.map(eachItem => renderDrawerItem(eachItem))}</List>
      </span>

      <Divider className={classes.divider} />

      <span className={classes.footer}>
        {/* Footer Title */}
        <Typography variant="button" className={classes.folderDetailsTitle}>
          Folder details
        </Typography>

        <FolderDetails {...props} />
      </span>
    </div>
  )

  return (
    <nav className={classes.drawer} aria-label="mailbox folders">
      <Hidden smUp implementation="css">
        <Drawer
          variant="temporary"
          anchor={"left"}
          open={props.mobileOpen}
          onClose={props.handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          {drawer}
        </Drawer>
      </Hidden>

      <Hidden xsDown implementation="css">
        <Drawer
          classes={{
            paper: classes.drawerPaper,
          }}
          variant="permanent"
          open
        >
          {drawer}
        </Drawer>
      </Hidden>
    </nav>
  )
}

export default SideBar
