import React, { useState } from "react"
import { makeStyles } from "@material-ui/core/styles"
import ReplayRoundedIcon from "@material-ui/icons/ReplayRounded"
import CheckRoundedIcon from "@material-ui/icons/CheckRounded"
import Typography from "@material-ui/core/Typography"
import Box from "@material-ui/core/Box"
import DayPicker from "react-day-picker"
import clsx from "clsx"
import "react-day-picker/lib/style.css"

import { dayStart } from "../utils/utils"

const useStyles = makeStyles(theme => ({
  root: {
    color:
      theme.props.mode === "dark" ? "rgba(255,255,255,0.8)" : "rgba(0,0,0,0.7)",
    fontSize: 10,
    borderWidth: "1px",
    borderStyle: "solid",
    borderColor:
      theme.props.mode === "dark"
        ? "rgba(255, 255, 255, 0.5)"
        : "rgba(0,0,0,0.5)",
    borderRadius: theme.spacing(1),
    paddingBottom: theme.spacing(2),
  },
  actions: {
    padding: theme.spacing(0, 3),
    display: "flex",
    justifyContent: "space-between",
  },
  buttons: {
    cursor: "pointer",
    display: "flex",
    width: "max-content",
    borderRadius: theme.spacing(0.5),
    padding: theme.spacing(0.5, 1),
    columnGap: theme.spacing(0.5),
    borderWidth: "1px",
    borderStyle: "solid",
    borderColor:
      theme.props.mode === "dark"
        ? "rgba(255, 255, 255, 0.5)"
        : "rgba(0,0,0,0.5)",
    "&:hover": {
      borderColor: "#01aeba",
      color: "#01aeba",
    },
  },
  buttonText: {
    fontSize: 13,
  },
  buttonIcon: {
    height: theme.spacing(2),
    width: theme.spacing(2),
  },
  blueButton: {
    color: "white",
    backgroundColor: "#01aeba",
    borderColor: "#01aeba",
    "&:hover": {
      color: "black",
    },
  },
}))

function DatePicker(props) {
  const classes = useStyles()
  const [selectedDay, setSelectedDay] = useState(new Date(props.day))
  const [choseDay, setChoseDay] = useState(false)

  console.log("initial day => ", props.day)

  const saveSelectedDay = (day, { disabled }) => {
    if (disabled) {
      return
    }

    console.log("me setting => ", new Date(day))
    setSelectedDay(new Date(day))
    setChoseDay(false)
  }

  const handleSetDay = () => {
    props.saveSharableUntil(dayStart(selectedDay))
    setChoseDay(true)
  }

  const handleReset = () => {
    setSelectedDay(new Date(props.day))
    setChoseDay(false)
  }

  const isDisabled = day => {
    let date = dayStart(day, true)
    let today = dayStart(new Date(), true)
    return date < today
  }

  return (
    <Box className={classes.root}>
      <DayPicker
        disabledDays={isDisabled}
        onDayClick={saveSelectedDay}
        selectedDays={selectedDay}
      />
      <Box className={classes.actions}>
        <Box
          className={
            choseDay
              ? clsx(classes.buttons, classes.blueButton)
              : classes.buttons
          }
          onClick={handleSetDay}
        >
          <CheckRoundedIcon className={classes.buttonIcon} />
          <Typography className={classes.buttonText}>Set</Typography>
        </Box>
        <Box className={classes.buttons} onClick={handleReset}>
          <ReplayRoundedIcon className={classes.buttonIcon} />
          <Typography className={classes.buttonText}>Reset</Typography>
        </Box>
      </Box>
    </Box>
  )
}

export default DatePicker
