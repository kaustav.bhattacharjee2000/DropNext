import React, { useState, Fragment } from "react"
import { Link } from "gatsby"
import { useLocation } from "@reach/router"
import { makeStyles } from "@material-ui/core/styles"
import FolderIcon from "@material-ui/icons/Folder"
import DescriptionRoundedIcon from "@material-ui/icons/DescriptionRounded"
import FilterListRoundedIcon from "@material-ui/icons/FilterListRounded"
import CircularProgress from "@material-ui/core/CircularProgress"
import Typography from "@material-ui/core/Typography"
import Table from "@material-ui/core/Table"
import TableBody from "@material-ui/core/TableBody"
import TableCell from "@material-ui/core/TableCell"
import TableContainer from "@material-ui/core/TableContainer"
import TableHead from "@material-ui/core/TableHead"
import TableRow from "@material-ui/core/TableRow"
import Grow from "@material-ui/core/Grow"
import Zoom from "@material-ui/core/Zoom"
import Box from "@material-ui/core/Box"
import queryString from "query-string"
import clsx from "clsx"
import { nanoid } from "nanoid"
import { useQuery } from "react-query"

import FileInfo from "./FileInfo"
import { simplifyFileSize } from "../utils/utils"
import { useNotification } from "../contexts/notification-context"
import { useAuth } from "../contexts/auth-context"
import * as SearchAPI from "../api/search"
import oneTheme from "../utils/theme"
import { DESKTOP_FILTERS_WIDTH } from "../utils/constants"

const useStyles = makeStyles(theme => ({
  root: {
    flex: 1,
    padding: theme.spacing(2, 0),
    display: "flex",
    flexFlow: "column",
    rowGap: theme.spacing(2),
    color: oneTheme.search.text[theme.props.mode],
  },
  topTitle: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  item: {
    display: "flex",
    alignItems: "center",
    columnGap: theme.spacing(1),
    padding: theme.spacing(1, 0),
  },
  subtitle: {
    fontSize: 14,
    color: oneTheme.search.subtitle[theme.props.mode],
  },
  icon: {
    width: theme.spacing(5),
    height: theme.spacing(5),
  },
  folderIcon: {
    color: "#01aeba",
  },
  fileIcon: {
    color: "#01c4d4",
  },
  toggleButtonGroup: {
    display: "flex",
    width: "100%",
    borderRadius: 5,
    overflow: "hidden",
    borderColor: oneTheme.search.desktopSelectedFilterBg[theme.props.mode],
    borderWidth: "0px 0.5px",
    borderStyle: "solid",
  },
  toggleButton: {
    backgroundColor: oneTheme.search.toggleButtonBg[theme.props.mode],
    borderColor: oneTheme.search.desktopSelectedFilterBg[theme.props.mode],
    borderWidth: "1px 0.5px",
    borderStyle: "solid",
    color: oneTheme.search.toggleButtonText[theme.props.mode],
    fontSize: 14,
    textTransform: "none",
    padding: theme.spacing(0.5, 1.25),
    cursor: "pointer",
    textAlign: "center",
  },
  selected: {
    backgroundColor: oneTheme.search.desktopSelectedFilterBg[theme.props.mode],
    color: "white",
  },
  filter: {
    padding: theme.spacing(2),
    borderRadius: theme.spacing(1),
    display: "flex",
    flexFlow: "column",
    rowGap: theme.spacing(1.5),
    backgroundColor: oneTheme.joinPage.formSmAbove[theme.props.mode],
    boxShadow: "0 4px 20px -8px rgba(0,0,0,0.4)",
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  filterItem: {
    display: "flex",
    flexFlow: "column",
    rowGap: theme.spacing(1),
  },
  filterApplyGroup: {
    display: "flex",
    columnGap: theme.spacing(3),
    marginTop: theme.spacing(1),
  },
  filterApplyButton: {
    flex: 1,
    borderRadius: theme.spacing(0.5),
    border: oneTheme.search.filterApplyButtonBorder[theme.props.mode],
    color: oneTheme.search.filterApplyButtonText[theme.props.mode],
    fontSize: 14,
    textTransform: "none",
    padding: theme.spacing(0.75, 1.25),
    cursor: "pointer",
    textAlign: "center",
  },
  disabledButton: {
    border: oneTheme.search.disabledButtonBorder[theme.props.mode],
    color: oneTheme.search.disabledButtonText[theme.props.mode],
  },
  filterIcon: {
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  mobileOnly: {
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  desktopOnly: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  desktopSearchResults: {
    flexGrow: 1,
    position: "relative",
    minHeight: theme.spacing(32),
  },
  tableHead: {
    backgroundColor: oneTheme.search.tableHeadBackground[theme.props.mode],
    position: "relative",
  },
  tableTitle: {
    fontWeight: "bold",
    color: "white",
  },
  tableText: {
    color: oneTheme.search.tableText[theme.props.mode],
  },
  desktopFilterButton: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      columnGap: theme.spacing(1),
      padding: theme.spacing(1, 2),
      borderRadius: theme.spacing(1),
      border: oneTheme.search.desktopFilterButtonBorder[theme.props.mode],
      color: oneTheme.search.text[theme.props.mode],
      cursor: "pointer",
      "&:hover": {
        border: "2px solid #01aeba",
        color: "#01aeba",
      },
    },
  },
  desktopFilterSection: {
    zIndex: 100,
    width: DESKTOP_FILTERS_WIDTH,
    position: "absolute",
    right: 0,
    top: 0,
    padding: theme.spacing(3),
    borderRadius: theme.spacing(2),
    backgroundColor: oneTheme.search.desktopFilterSectionBg[theme.props.mode],
    display: "flex",
    flexFlow: "column",
    rowGap: theme.spacing(2),
    boxShadow: "0 4px 20px -8px rgba(0,0,0,0.4)",
  },
  desktopFilterSectionTitle: {
    fontWeight: "bold",
    color: oneTheme.search.text[theme.props.mode],
    padding: theme.spacing(0.25, 1),
  },
  desktopFilters: {
    display: "flex",
    columnGap: theme.spacing(2),
  },
  desktopFilterColumn: {
    flex: 1,
    color: oneTheme.search.tableText[theme.props.mode],
  },
  desktopSelectedFilter: {
    backgroundColor: oneTheme.search.desktopSelectedFilterBg[theme.props.mode],
    color: "white",
    padding: theme.spacing(0.25, 1),
    borderRadius: theme.spacing(0.5),
    cursor: "pointer",
  },
  desktopFilterText: {
    color: "inherit",
    padding: theme.spacing(0.25, 1),
    borderRadius: theme.spacing(0.5),
    cursor: "pointer",
    "&:hover": {
      color: "#01aeba",
    },
  },
}))

// @helper function to create "id" field
// from a search result object "obj"
function makeProperObject(obj) {
  const [[key, value]] = Object.entries(obj)
  value.id = key
  return value
}

// @helper function to make all files come
// after all folders
function segregateByType(list) {
  let files = []
  let folders = []

  list.forEach(item => {
    if (item.type === "file") {
      files.push(item)
    } else {
      folders.push(item)
    }
  })

  return folders.concat(files)
}

// @helper function to convert each search result
// in list as per ease of displaying
function transformResult(resultList) {
  return segregateByType(resultList.map(object => makeProperObject(object)))
}

// @helper function to check if properties of
// two objects have changed
function hasObjectChanged(objA, objB) {
  let entriesA = Object.entries(objA)
  let entriesB = Object.entries(objB)
  for (let entry of entriesA) {
    let [key, value] = entry
    if (objB[key] !== value) {
      return true
    }
  }
  return entriesA.length !== entriesB.length
}

// @helper function for toggle case
function toggleCase(text) {
  return text
    .split(" ")
    .map(word => word[0].toUpperCase() + word.substring(1))
    .join(" ")
}

function TopTitle(props) {
  const classes = useStyles()

  return (
    <Box className={classes.topTitle}>
      <Typography component="span">
        Found{" "}
        <Typography display="inline" style={{ fontWeight: "bold" }}>
          {props.nResults}
        </Typography>{" "}
        results
      </Typography>

      <FilterListRoundedIcon
        onClick={props.toggleFilter}
        className={classes.filterIcon}
      />

      <Box className={classes.desktopFilterButton} onClick={props.toggleFilter}>
        <FilterListRoundedIcon />
        <Typography>Filters</Typography>
        {props.children}
      </Box>
    </Box>
  )
}

function ToggleButton(props) {
  const classes = useStyles()
  const id = nanoid(3)

  return (
    <Box className={classes.toggleButtonGroup}>
      {props.keys.map((key, index) => (
        <Box
          key={`${id}_${index}`}
          flex={1}
          onClick={() => props.changeFilter(props.filterType, key)}
          className={
            props.selected === key
              ? clsx(classes.toggleButton, classes.selected)
              : classes.toggleButton
          }
        >
          {key}
        </Box>
      ))}
    </Box>
  )
}

function DetailsWrapper(props) {
  const [fileInfoVisible, setFileInfoVisible] = useState(false)

  const handleFileInfoClose = () => {
    setFileInfoVisible(false)
  }

  const handleFileInfoOpen = () => {
    setFileInfoVisible(true)
  }

  const FileWrapper = ({ children }) => (
    <Fragment>
      <Box style={{ cursor: "pointer" }} onClick={handleFileInfoOpen}>
        {children}
      </Box>
      <FileInfo
        open={fileInfoVisible}
        onClose={handleFileInfoClose}
        fileId={props.id}
      />
    </Fragment>
  )

  const FolderWrapper = ({ children }) => (
    <Link
      to={`/folder/${props.id}`}
      style={{ textDecoration: "none", color: "inherit" }}
    >
      {children}
    </Link>
  )

  return props.type === "file" ? (
    <FileWrapper>{props.children}</FileWrapper>
  ) : (
    <FolderWrapper>{props.children}</FolderWrapper>
  )
}

function MobileFilterSection(props) {
  const classes = useStyles()
  const initialFilters = props.filters

  const [filters, setFilters] = useState(initialFilters)

  // Gives key to filter mapping. "key" is what is shown as button name
  // like Name, Size, All, Folders etc. "filter" is what stored in state
  // and used for filtering like name, [file]. "filterType" is sortBy,
  // type and root.
  let keyToFilter = {
    sortBy: {
      Name: "name",
      Size: "size",
    },
    type: {
      All: ["file", "folder"],
      Folders: ["folder"],
      Files: ["file"],
    },
    root: {
      All: ["Drive", "Trash"],
      Drive: ["Drive"],
      Trash: ["Trash"],
    },
  }

  // Reverse maps filter to key
  const filterToKey = (filterType, filter) => {
    let mapping = keyToFilter[filterType]
    let [key] = Object.entries(mapping).find(([key, value]) => {
      if (typeof value === "string") {
        return value === filter
      }
      return (
        value.length === filter.length && value.every(v => filter.includes(v))
      )
    })
    return key
  }

  const changeFilter = (filterType, key) => {
    let updateFilter = {}
    updateFilter[filterType] = keyToFilter[filterType][key]
    setFilters(prev => ({ ...prev, ...updateFilter }))
  }

  const handleApply = () => {
    props.applyFilters(filters)
  }

  const handleCancel = () => {
    setFilters(initialFilters)
    props.applyFilters(initialFilters)
  }

  const hasChanged = hasObjectChanged(filters, initialFilters)

  return (
    <Zoom in timeout={500}>
      <Box className={classes.filter}>
        <Box className={classes.filterItem}>
          <Typography>Sort by</Typography>
          <ToggleButton
            keys={["Name", "Size"]}
            selected={filterToKey("sortBy", filters.sortBy)}
            filterType="sortBy"
            changeFilter={changeFilter}
          />
        </Box>

        <Box className={classes.filterItem}>
          <Typography>Type</Typography>
          <ToggleButton
            keys={["All", "Folders", "Files"]}
            selected={filterToKey("type", filters.type)}
            filterType="type"
            changeFilter={changeFilter}
          />
        </Box>

        <Box className={classes.filterItem}>
          <Typography>Root location</Typography>
          <ToggleButton
            keys={["All", "Drive", "Trash"]}
            selected={filterToKey("root", filters.root)}
            filterType="root"
            changeFilter={changeFilter}
          />
        </Box>

        <Box className={classes.filterApplyGroup}>
          <Box
            onClick={hasChanged ? handleApply : null}
            className={
              hasChanged
                ? classes.filterApplyButton
                : clsx(classes.filterApplyButton, classes.disabledButton)
            }
          >
            Apply
          </Box>
          <Box className={classes.filterApplyButton} onClick={handleCancel}>
            Cancel
          </Box>
        </Box>
      </Box>
    </Zoom>
  )
}

function MobileSearchedItem({ content, ...restProps }) {
  const classes = useStyles()

  return (
    <Box className={classes.item} {...restProps}>
      {content.type === "file" ? (
        <DescriptionRoundedIcon
          className={clsx(classes.icon, classes.fileIcon)}
        />
      ) : (
        <FolderIcon className={clsx(classes.icon, classes.folderIcon)} />
      )}
      <Box>
        <DetailsWrapper {...content}>
          <Typography style={{ fontWeight: "bold" }}>{content.name}</Typography>
        </DetailsWrapper>
        <Typography className={classes.subtitle}>
          {content.size
            ? `${simplifyFileSize(content.size)} / ${toggleCase(content.root)}`
            : `${toggleCase(content.root)}`}
        </Typography>
      </Box>
    </Box>
  )
}

function MobileSearchResults(props) {
  const classes = useStyles()

  return (
    <Box className={classes.mobileOnly}>
      {props.data.map(content => (
        <MobileSearchedItem key={content.id} content={content} />
      ))}
    </Box>
  )
}

function DesktopFilterSection(props) {
  const classes = useStyles()
  const initialFilters = props.filters
  const [filters, setFilters] = useState(initialFilters)

  let keyToFilter = {
    sortBy: {
      Name: "name",
      Size: "size",
    },
    type: {
      All: ["file", "folder"],
      Folders: ["folder"],
      Files: ["file"],
    },
    root: {
      All: ["Drive", "Trash"],
      Drive: ["Drive"],
      Trash: ["Trash"],
    },
  }

  const filterToKey = (filterType, filter) => {
    let mapping = keyToFilter[filterType]
    let [key] = Object.entries(mapping).find(([key, value]) => {
      if (typeof value === "string") {
        return value === filter
      }
      return (
        value.length === filter.length && value.every(v => filter.includes(v))
      )
    })
    return key
  }

  const changeFilter = (filterType, key) => {
    let updateFilter = {}
    updateFilter[filterType] = keyToFilter[filterType][key]
    setFilters(prev => ({ ...prev, ...updateFilter }))
  }

  const handleApply = () => {
    props.applyFilters(filters)
  }

  const handleCancel = () => {
    setFilters(initialFilters)
    props.applyFilters(initialFilters)
  }

  const hasChanged = hasObjectChanged(filters, initialFilters)

  const FilterButton = ({ filterType, name }) => {
    const isSelected = filterToKey(filterType, filters[filterType]) === name

    return (
      <Typography
        className={
          isSelected ? classes.desktopSelectedFilter : classes.desktopFilterText
        }
        onClick={() => changeFilter(filterType, name)}
      >
        {name}
      </Typography>
    )
  }

  const FilterTitle = ({ title }) => (
    <Typography className={classes.desktopFilterSectionTitle}>
      {title}
    </Typography>
  )

  return (
    <Grow in timeout={800}>
      <Box className={classes.desktopFilterSection}>
        <Box className={classes.desktopFilters}>
          <Box className={classes.desktopFilterColumn}>
            <FilterTitle title={"Sort by"} />
            <FilterButton filterType={"sortBy"} name={"Name"} />
            <FilterButton filterType={"sortBy"} name={"Size"} />
          </Box>
          <Box className={classes.desktopFilterColumn}>
            <FilterTitle title={"Type"} />
            <FilterButton filterType={"type"} name={"All"} />
            <FilterButton filterType={"type"} name={"Files"} />
            <FilterButton filterType={"type"} name={"Folders"} />
          </Box>
          <Box className={classes.desktopFilterColumn}>
            <FilterTitle title={"Root"} />
            <FilterButton filterType={"root"} name={"All"} />
            <FilterButton filterType={"root"} name={"Drive"} />
            <FilterButton filterType={"root"} name={"Trash"} />
          </Box>
        </Box>

        <Box className={classes.filterApplyGroup}>
          <Box
            onClick={hasChanged ? handleApply : null}
            className={
              hasChanged
                ? classes.filterApplyButton
                : clsx(classes.filterApplyButton, classes.disabledButton)
            }
          >
            Apply
          </Box>
          <Box onClick={handleCancel} className={classes.filterApplyButton}>
            Cancel
          </Box>
        </Box>
      </Box>
    </Grow>
  )
}

function Cell({ className, children, left, right }) {
  const borders = (left, right) =>
    left
      ? { borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }
      : right
      ? { borderTopRightRadius: 10, borderBottomRightRadius: 10 }
      : {}

  return (
    <TableCell style={{ borderBottom: 0, ...borders(left, right) }}>
      <Box style={{ display: "flex", columnGap: 5 }}>
        <Typography className={className}>{children}</Typography>
      </Box>
    </TableCell>
  )
}

function DesktopSearchResults(props) {
  const classes = useStyles()

  return (
    <Box className={classes.desktopOnly}>
      <TableContainer className={classes.desktopSearchResults}>
        <Table aria-label="search-results">
          <TableHead className={classes.tableHead}>
            <TableRow>
              <Cell className={classes.tableTitle} left>
                Name
              </Cell>
              <Cell className={classes.tableTitle}>Size</Cell>
              <Cell className={classes.tableTitle}>Type</Cell>
              <Cell className={classes.tableTitle} right>
                Root
              </Cell>
            </TableRow>
          </TableHead>

          {props.showFilters && (
            <DesktopFilterSection
              applyFilters={props.applyFilters}
              filters={props.filters}
            />
          )}

          <TableBody>
            {props.data.map(content => (
              <TableRow key={content.id}>
                <Cell style={{ borderBottom: 0 }} className={classes.tableText}>
                  <DetailsWrapper {...content}>{content.name}</DetailsWrapper>
                </Cell>
                <Cell className={classes.tableText}>
                  {content.size ? simplifyFileSize(content.size) : "-"}
                </Cell>
                <Cell className={classes.tableText}>
                  {toggleCase(content.type)}
                </Cell>
                <Cell className={classes.tableText}>
                  {toggleCase(content.root)}
                </Cell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  )
}

function DisplayLoading(props) {
  const classes = useStyles()

  return (
    <Box style={{ display: "flex", alignItems: "center", columnGap: 10 }}>
      <CircularProgress className={classes.folderIcon} />
      <Typography className={classes.tableText} style={{ fontSize: 20 }}>
        Searching ...
      </Typography>
    </Box>
  )
}

const initialFilters = {
  sortBy: "name",
  type: ["file", "folder"],
  root: ["Drive", "Trash"],
}

function Search(props) {
  const classes = useStyles()
  const location = useLocation()
  const { firebase, user } = useAuth()
  const { showNotification } = useNotification()

  const [showFilters, setShowFilters] = useState(false)
  const [filteredData, setFilteredData] = useState(null)
  const [filters, setFilters] = useState(initialFilters)

  const searchText = queryString.parse(location.search).search.toLowerCase()

  const { data, status } = useQuery(
    [searchText, searchText],
    ({ queryKey }) => SearchAPI.search(firebase, queryKey[1], user && user.id),
    {
      refetchOnWindowFocus: false,
      onSuccess: data => {
        console.log("onSuccess => ", data)
        setFilteredData(transformResult(data))
      },
      onError: error => {
        console.error(`Error while searching : ${searchText}`)
        console.error("Details => ", error)
        showNotification({ type: "error", message: "Failed to search" })
      },
    }
  )

  const toggleShowFilters = () => {
    setShowFilters(prev => !prev)
  }

  const applyFilters = updatedFilters => {
    console.log("initial data => ", transformResult(data))
    console.log("updated filters => ", updatedFilters)
    let transformed = transformResult(data)

    let previousData = transformed.filter(
      item =>
        updatedFilters.type.includes(item.type) &&
        updatedFilters.root.includes(item.root)
    )
    let folders = previousData.filter(item => item.type === "folder")
    let files = previousData.filter(item => item.type === "file")

    if (updatedFilters.sortBy === "name") {
      // sort by name, sort files and folders separately & lexicographically
      folders = folders.sort((itemA, itemB) =>
        itemA.name.localeCompare(itemB.name)
      )
      files = files.sort((itemA, itemB) => itemA.name.localeCompare(itemB.name))
    } else {
      // sort by size, only sort the files
      files = files.sort((itemA, itemB) => itemA.size - itemB.size)
    }

    // merge the results, files after folders
    let updatedData = folders.concat(files)

    setFilters(updatedFilters)
    setFilteredData(updatedData)

    console.log("updated data => ", updatedData)
    return updatedData
  }

  return (
    <Box className={classes.root}>
      {status === "loading" ? (
        <DisplayLoading />
      ) : (
        filteredData && (
          <Fragment>
            <TopTitle
              nResults={filteredData.length}
              toggleFilter={toggleShowFilters}
            />
            {showFilters && (
              <MobileFilterSection
                filters={filters}
                applyFilters={applyFilters}
              />
            )}
            <MobileSearchResults data={filteredData} />
            <DesktopSearchResults
              data={filteredData}
              showFilters={showFilters}
              applyFilters={applyFilters}
              filters={filters}
            />
          </Fragment>
        )
      )}
    </Box>
  )
}

export default Search
