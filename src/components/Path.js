import React, { Fragment } from "react"
import { Link } from "gatsby"
import { useTheme } from "@material-ui/core/styles"
import Divider from "@material-ui/core/Divider"
import NavigateNextIcon from "@material-ui/icons/NavigateNext"
import Breadcrumbs from "@material-ui/core/Breadcrumbs"
import Typography from "@material-ui/core/Typography"

import oneTheme from "../utils/theme"

function Path({ folderMetaData, ...restProps }) {
  const theme = useTheme()

  if (!folderMetaData || !folderMetaData.path) {
    return null
  }

  // "path" in folder's meta data doesn't include itself,
  // so adding an extra entry since it needs to be shown
  // on breadcrumbs
  const path = [
    ...folderMetaData.path,
    { id: folderMetaData.id, name: folderMetaData.name },
  ]

  return (
    <Fragment>
      <Breadcrumbs
        aria-label="breadcrumb"
        separator={
          <NavigateNextIcon
            fontSize="small"
            htmlColor={oneTheme.path.inactive[theme.props.mode]}
          />
        }
        {...restProps}
      >
        {path.map((folder, index) => (
          <Link
            key={`breadcrumb_${folder.id}`}
            to={`/folder/${folder.id}`}
            style={{
              textDecoration: "inherit",
              color: oneTheme.path.inactive[theme.props.mode],
            }}
          >
            <Typography
              key={folder.id}
              style={{
                color:
                  index === path.length - 1
                    ? oneTheme.path.active[theme.props.mode]
                    : "inherit",
              }}
            >
              {folder.name}
            </Typography>
          </Link>
        ))}
      </Breadcrumbs>
      <Divider
        style={{
          backgroundColor: oneTheme.path.inactive[theme.props.mode],
          marginBottom: theme.spacing(1),
        }}
      />
    </Fragment>
  )
}

export default Path
