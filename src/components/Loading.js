import React from "react"
import CircularProgress from "@material-ui/core/CircularProgress"
import Box from "@material-ui/core/Box"
import { makeStyles } from "@material-ui/core/styles"

const useStyles = makeStyles(theme => ({
  loadingWrapper: {
    display: "flex",
    justifyContent: "center",
  },
  loading: {
    color: "#03b7c2",
  },
}))

function Loading({ margin, thickness, size }) {
  const classes = useStyles()

  return (
    <Box m={margin} className={classes.loadingWrapper}>
      <CircularProgress
        thickness={thickness}
        size={size}
        className={classes.loading}
      />
    </Box>
  )
}

export default Loading
