import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import IconButton from "@material-ui/core/IconButton"
import Avatar from "@material-ui/core/Avatar"
import Button from "@material-ui/core/Button"
import AccountCircleIcon from "@material-ui/icons/AccountCircle"
import Box from "@material-ui/core/Box"
import { Link } from "gatsby"
import clsx from "clsx"

import AppName from "./AppName"
import DarkThemeToggle from "./DarkModeToggle"
import { useAuth } from "../contexts/auth-context"

const useStyles = makeStyles(theme => ({
  appname: {
    width: 150,
    height: "auto",
  },
  title: {
    flexGrow: 1,
  },
  link: {
    textDecoration: "none",
    color: "inherit",
  },
  linkButton: {
    color: "white",
    fontWeight: "bold",
    backgroundColor: "#03b7c2",
    boxShadow: "none",
    padding: theme.spacing(0.5),
    "&:hover": {
      backgroundColor: "#02929b",
      boxShadow: "none",
    },
  },
  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
  },
  header: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  icon: {
    width: 28,
    height: 28,
    color: "#01aeba",
  },
  navbarRight: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    columnGap: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      columnGap: theme.spacing(4),
    },
  },
  mobileOnly: {
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  desktopOnly: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
}))

const NavBar = props => {
  const classes = useStyles()
  const { user } = useAuth()

  return (
    <header className={clsx(classes.header, props.containerStyle || {})}>
      <Box className={classes.title}>
        <Link to="/" className={classes.link}>
          <AppName fill="#01c4d4" className={classes.appname} />
        </Link>
      </Box>

      <Box className={classes.navbarRight}>
        <DarkThemeToggle />
        {!user ? (
          <Link to="/join" className={classes.link}>
            <Button
              variant="contained"
              className={clsx(classes.linkButton, classes.desktopOnly)}
            >
              Join
            </Button>

            <AccountCircleIcon
              className={clsx(classes.icon, classes.mobileOnly)}
            />
          </Link>
        ) : (
          <Link to={`/folder/${user.drive}`} className={classes.link}>
            <IconButton aria-label="profile" size="small">
              <Avatar className={classes.avatar} src={user.avatar}>
                {user && user.displayName && user.displayName[0]}
              </Avatar>
            </IconButton>
          </Link>
        )}
      </Box>
    </header>
  )
}

export default NavBar
