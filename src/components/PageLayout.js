import React from "react"
import {
  MuiThemeProvider,
  createMuiTheme,
  responsiveFontSizes,
} from "@material-ui/core/styles"
import { QueryClientProvider, QueryClient } from "react-query"
import { ReactQueryDevtools } from "react-query/devtools"

import Seo from "./Seo"
import { NotificationProvider } from "../contexts/notification-context"
import Notification from "../components/Notification"
import { useAuth } from "../contexts/auth-context"

const queryClient = new QueryClient()

function PageLayout({ title, children, allowNotification, position }) {
  const { settings } = useAuth()

  const baseTheme = responsiveFontSizes(
    createMuiTheme({
      typography: {
        fontFamily: ["Nunito Sans", "Roboto", "sans-serif"].join(","),
        color: "inherit",
      },
      props: {
        mode: settings.darkMode ? "dark" : "light",
      },
    })
  )

  return (
    <MuiThemeProvider theme={baseTheme}>
      <NotificationProvider
        allowNotification={allowNotification}
        position={position}
      >
        <QueryClientProvider client={queryClient}>
          <Seo title={title} />
          <ReactQueryDevtools initialIsOpen={false} />
          <Notification />
          {children}
        </QueryClientProvider>
      </NotificationProvider>
    </MuiThemeProvider>
  )
}

export default PageLayout
