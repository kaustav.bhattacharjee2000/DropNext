import React, {
  useEffect,
  createContext,
  useState,
  useMemo,
  useContext,
} from "react"

import useFirebase, {
  googleSignInWithRedirect,
  localSignIn,
  getDatabase,
} from "../utils/firebase"

import { MAX_DISPLAY_NAME_LENGTH } from "../utils/constants"

const AuthContext = createContext()
const windowGlobal = typeof window !== "undefined" && window

const defaultSettings = {
  darkMode: false,
  notificationSounds: true,
}

function AuthProvider({ children }) {
  const [loading, setLoading] = useState(false)
  const [user, setUser] = useState(null)
  const [settings, setSettings] = useState(getSettingsFromLocalStorage())
  const firebase = useFirebase()

  useEffect(() => {
    if (!firebase) {
      return
    }

    const unsubscribe = async () => {
      setLoading(true)
      firebase.auth().onAuthStateChanged(getUserData)
    }

    unsubscribe()

    return () => unsubscribe()
  }, [firebase])

  async function getUserData(currentUser) {
    if (currentUser) {
      console.log("CURRENT USER ========> ", currentUser)

      let minimalUser = {
        id: currentUser.uid,
        avatar: currentUser.photoURL,
        email: currentUser.email,
        displayName: currentUser.displayName,
        isGoogleAuthenticated:
          currentUser.providerData[0].providerId === "google.com",
      }

      const workspaceData = await getUserWorkspace(minimalUser)
      minimalUser = { ...minimalUser, ...workspaceData }

      setUser(minimalUser)
      setLoading(false)
    } else {
      setUser(null)
      setLoading(false)
    }
  }

  function getSettingsFromLocalStorage() {
    // To prevent failing in production build stage
    if (!windowGlobal) {
      return defaultSettings
    }

    let darkMode = localStorage.getItem("darkMode")
    let notificationSounds = localStorage.getItem("notificationSounds")

    if (!darkMode) {
      darkMode = defaultSettings.darkMode
      localStorage.setItem("darkMode", darkMode)
    }

    if (!notificationSounds) {
      notificationSounds = defaultSettings.notificationSounds
      localStorage.setItem("notificationSounds", notificationSounds)
    }

    let settings = {
      darkMode: JSON.parse(darkMode),
      notificationSounds: JSON.parse(notificationSounds),
    }

    return settings
  }

  async function googleAuth() {
    const result = await googleSignInWithRedirect()
    return result
  }

  async function login(email, password) {
    const result = await localSignIn(email, password)
    return result
  }

  async function signup(displayName, email, password) {
    if (!firebase || !windowGlobal) {
      return
    }

    // Saving display name as state won't work because
    // redirecting to google auth will erase it, instead
    // saving it temporarily in local storage, and
    // accessing it later on
    localStorage.setItem("tempDisplayName", displayName)

    return firebase.auth().createUserWithEmailAndPassword(email, password)
  }

  async function logout() {
    if (!firebase) {
      return
    }

    await firebase.auth().signOut()
  }

  // Creates a user's workspace in firebase containing drive
  // & trash folder, and a user document for meta data
  async function createUserWorkspace(user) {
    if (!firebase) {
      return
    }

    const { users, getCurrentTimestamp } = getDatabase()

    // Creating the drive & trash folders beforehand to get ids
    const driveFolder = await createRootFolder("Drive", user.id)
    const trashFolder = await createRootFolder("Trash", user.id)

    // Trim the user's displayName if authenticating via google sign-in
    const trimmedDisplayname =
      user.displayName && user.displayName.substr(0, MAX_DISPLAY_NAME_LENGTH)

    const userStructure = {
      displayName:
        trimmedDisplayname || localStorage.getItem("tempDisplayName"),
      drive: driveFolder.id,
      trash: trashFolder.id,
      joinedOn: getCurrentTimestamp(),
    }

    await users.doc(user.id).set(userStructure)
    localStorage.removeItem("tempDisplayName")

    const createdUser = await users.doc(user.id).get()
    return createdUser.data()
  }

  async function getUserWorkspace(user) {
    // fetch the user first from "users" collection
    const { users } = getDatabase()

    const userRef = await users.doc(user.id).get()

    if (!userRef.exists) {
      // maybe due to some fault user's workspace wasn't
      // created, create the workspace now
      return createUserWorkspace(user)
    }

    return userRef.data()
  }

  async function createRootFolder(folderName, ownerId, path = []) {
    if (!firebase) {
      return
    }

    const { folders, getCurrentTimestamp } = getDatabase()

    const folderStructure = {
      name: folderName,
      path: path,
      owner: ownerId,
      files: {},
      folders: {},
      createdAt: getCurrentTimestamp(),
      modifiedAt: getCurrentTimestamp(),
    }

    const createdFolderRef = await folders.add(folderStructure)
    const createdFolder = await createdFolderRef.get()
    return { ...createdFolder.data(), id: createdFolderRef.id }
  }

  async function updateDisplayName(newName) {
    const { users } = getDatabase()

    const userRef = users.doc(user.id)
    await userRef.update({ displayName: newName })
    setUser(prevUser => ({ ...prevUser, displayName: newName }))
  }

  function updateSettings(newSettings) {
    // To prevent failing in production build stage
    if (!windowGlobal) {
      return
    }

    Object.entries(newSettings).forEach(([key, value]) => {
      localStorage.setItem(key, value)
    })

    setSettings(prevSettings => ({ ...prevSettings, ...newSettings }))
  }

  const memoisedValue = useMemo(
    () => ({
      firebase,
      user,
      settings,
      googleAuth,
      login,
      signup,
      logout,
      updateDisplayName,
      updateSettings,
      loading,
    }),
    [user, settings, loading, firebase]
  )

  return (
    <AuthContext.Provider value={memoisedValue}>
      {children}
    </AuthContext.Provider>
  )
}

function useAuth(props) {
  return useContext(AuthContext)
}

export { AuthProvider, useAuth }
