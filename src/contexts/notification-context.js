import React, { createContext, useState, useMemo, useContext } from "react"

const NotificationContext = createContext()

/**
 * To show notification, a sample object to be passed will look like :
 * { type: "success", message: "task done !"}
 *
 * Currently only one notification will be shown on screen and if
 * multiple are called then only the last one will be shown
 */

function NotificationProvider({ children, allowNotification, position }) {
  const [notification, setNotification] = useState(null)

  function showNotification(newNotification) {
    setNotification(newNotification)
  }

  function closeNotification() {
    setNotification(null)
  }

  const memoisedValue = useMemo(
    () => ({
      notification,
      showNotification,
      closeNotification,
      allowNotification,
      position,
    }),
    [notification, position, allowNotification]
  )

  return (
    <NotificationContext.Provider value={memoisedValue}>
      {children}
    </NotificationContext.Provider>
  )
}

function useNotification(props) {
  return useContext(NotificationContext)
}

export { NotificationProvider, useNotification }
