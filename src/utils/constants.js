// Fixed width for sidebar for all devices in px
export const SIDEBAR_WIDTH = 240

// Fixed width for folder dialog for desktop devices in px
export const FOLDER_MODAL_WIDTH = 450

// Fixed width for file description dialog for desktop devices in px
export const FILE_MODAL_WIDTH = 540

// Maximum uploading file size in bytes
export const MAX_UPLOAD_FILE_SIZE = 5 * 1024 * 1024 // 5 MB

// Maximum depth of nested folders
export const MAX_NESTED_FOLDERS_DEPTH = 20

// Maximum number of child folders in a folder
export const MAX_CHILD_FOLDERS = 20

// Maximum number of files in a folder
export const MAX_CHILD_FILES = 20

// Fixed length of short link in characters
export const SHORT_LINK_LENGTH = 8

// Maximum length of filename or foldername in characters
export const MAX_FNAME_LENGTH = 20

// Fixed width of desktop filter section in px
export const DESKTOP_FILTERS_WIDTH = 400

// Fixed width of form used in joining (both login & signup) in px
export const FORM_WIDTH = 384

// Maximum length of displayName in characters
export const MAX_DISPLAY_NAME_LENGTH = 25

// Maximum email length in characters
export const MAX_EMAIL_LENGTH = 320

// Maximum password length in characters
export const MAX_PASSWORD_LENGTH = 20

// Allowed characters in displayName
// @source : https://stackoverflow.com/a/23344557
export const ALLOWED_DISPLAY_NAME_CHARS = RegExp(
  /[~`!#$%^&*+=\-[\]\\';,/{}|\\":<>?]/
)

// Allowed characters in email (accepts unicode)
// @source : https://stackoverflow.com/a/46181
export const ALLOWED_EMAIL_CHARS = RegExp(
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
)

// Allowed characters in password
// @source : https://stackoverflow.com/a/11896930 (check in comments)
export const ALLOWED_PASSWORD_CHARS = RegExp(
  /[\s~`!@#$%^&*+=\-[\]\\';,/{}|\\":<>?()._]/
)
