import { useEffect, useState } from "react"
import firebase from "firebase/app"
import "firebase/auth"
import "firebase/firestore"
import "firebase/storage"
import "firebase/database"

const firebaseConfig = {
  apiKey: process.env.GATSBY_API_KEY,
  authDomain: process.env.GATSBY_AUTH_DOMAIN,
  databaseURL: process.env.GATSBY_DATABASE_URL,
  projectId: process.env.GATSBY_PROJECT_ID,
  storageBucket: process.env.GATSBY_STORAGE_BUCKET,
  messagingSenderId: process.env.GATSBY_MESSAGING_SENDER_ID,
  appId: process.env.GATSBY_APP_ID,
}

let firebaseCachedInstance

export const getFirebase = () => {
  // During production build it checks if window is
  // defined, otherwise it will result in error
  if (typeof window === "undefined") {
    return null
  }

  if (!firebaseCachedInstance) {
    // Check if config object has all key:value pairs defined
    Object.entries(firebaseConfig).forEach(([key, value]) => {
      if (!value) {
        throw new Error(`Key "${key}" has empty value in environment variables`)
      }
    })

    if (!firebase.apps.length) {
      firebaseCachedInstance = firebase.initializeApp(firebaseConfig)
    } else {
      // if already initialized, use that one
      firebaseCachedInstance = firebase.app()
    }
  }

  return firebaseCachedInstance
}

export const googleProvider = () => {
  let provider = new firebase.auth.GoogleAuthProvider()
  // provider.addScope("https://www.googleapis.com/auth/contacts.readonly")
  return provider
}

export const localPersistence = async () => {
  let firebaseInstance = getFirebase()

  // avoid production build errors
  if (!firebaseInstance) {
    return
  }

  firebaseInstance.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
}

export const googleSignInWithRedirect = async () => {
  let firebaseInstance = getFirebase()

  // avoid production build errors
  if (!firebaseInstance) {
    return
  }

  let provider = googleProvider()
  await localPersistence()
  return firebaseInstance.auth().signInWithRedirect(provider)
}

export const localSignIn = async (email, password) => {
  let firebaseInstance = getFirebase()

  // avoid production build errors
  if (!firebaseInstance) {
    return
  }

  return firebaseInstance.auth().signInWithEmailAndPassword(email, password)
}

export const getDatabase = () => {
  let firebaseInstance = getFirebase()

  // avoid production build errors
  if (!firebaseInstance) {
    return
  }

  const firestore = firebaseInstance.firestore()
  const realtime = firebaseInstance.database()

  const database = {
    users: firestore.collection("users"),
    folders: firestore.collection("folders"),
    files: firestore.collection("files"),
    getCurrentTimestamp: firebase.firestore.FieldValue.serverTimestamp,
    fieldValue: firebase.firestore.FieldValue,
    firestore: firestore,
    realtime: realtime,
  }

  return database
}

export const getStorage = () => {
  let firebaseInstance = getFirebase()

  // avoid production build errors
  if (!firebaseInstance) {
    return
  }

  return firebaseInstance.storage()
}

export default function useFirebase() {
  const [firebaseInstance, setFirebaseInstance] = useState(null)

  useEffect(() => {
    setFirebaseInstance(getFirebase())
  }, [])

  return firebaseInstance
}
