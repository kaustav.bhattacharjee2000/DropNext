let colors = {
  grey: {
    100: "white",
    200: "#e8ecef",
    400: "#8795a5",
    600: "#373740",
    700: "#21212b",
    800: "#181820",
    900: "black",
  },
  blue: {
    100: "#01c4d4",
    200: "#01aeba",
  },
}

const themeMaker = theme => {
  let spitTheme = {}
  Object.entries(theme).forEach(([key, value]) => {
    let newValue = {
      light: value[0],
      dark: value[value.length - 1],
      null: value[0], // edge case
      undefined: value[0], // edge case
    }
    spitTheme[key] = newValue
  })
  return spitTheme
}

const combineThemes = themes => {
  const combined = {}
  Object.entries(themes).forEach(([key, value]) => {
    combined[key] = themeMaker(value)
  })
  return combined
}

// array order : [light, dark]

let folderLayout = {
  background: [colors.grey[200], colors.grey[800]],
  searchBar: [colors.grey[100], "rgba(255,255,255,0.9)"],
}

let settings = {
  sectionBackground: [colors.grey[100], colors.grey[700]],
  sectionText: [colors.grey[400], "#86868b"],
  profileText: ["rgba(0,0,0,0.7)", "rgba(255,255,255,0.9)"],
  editBackground: ["#01aeba", "#414052"],
}

let path = {
  active: ["rgba(0,0,0,0.9)", "white"],
  inactive: ["rgba(0,0,0,0.5)", "#86868b"],
}

let sideBar = {
  background: ["white", "#21212b"],
  tabs: ["#8795a5", colors.grey[100]],
  separator: ["rgba(0,0,0,0.3)", "rgba(255,255,255,0.3)"],
  detailsTitle: ["#6a7d91", "#bcbcc0"],
  detailsContent: ["rgba(0,0,0,0.6)", "rgba(255,255,255,0.6)"],
  disabledButtonBorder: [
    "1px solid rgba(0,0,0,0.3)",
    "1px solid rgba(255,255,255,0.3)",
  ],
  disabledButtonText: ["rgba(0,0,0,0.3)", "rgba(255,255,255,0.3)"],
}

let customButton = {
  defaultText: ["rgba(0,0,0,0.6)", "rgba(255,255,255,0.8)"],
  defaultBorder: [
    "1px solid rgba(0,0,0,0.4)",
    "1px solid rgba(255,255,255,0.8)",
  ],
  hoverText: ["#03b7c2"],
  hoverBorder: ["1px solid #03b7c2"],
}

let folderContents = {
  itemBox: [colors.grey[100], colors.grey[700]],
  caption: ["#6a7d91", "#7a7986"],
  text: ["#0e2c4b", "rgba(255,255,255,0.8)"],
  italics: ["rgba(0,0,0,0.5)", "#414052"],
}

let fileInfo = {
  background: ["white", colors.grey[700]],
  text: ["black", "rgba(255,255,255,0.8)"],
  divider: ["rgba(0,0,0,0.5)", "rgba(255,255,255,0.3)"],
  visibilityButtonColor: ["rgba(0,0,0,0.7)", "rgba(255,255,255,0.5)"],
}

let darkModeToggle = {
  color: ["#e3c52d", "#86868b"],
}

let notification = {}

let search = {
  text: ["rgba(0,0,0,0.7)", "white"],
  subtitle: ["rgba(0,0,0,0.6)", "rgba(255,255,255,0.6)"],
  tableHeadBackground: ["rgba(55, 55, 64, 0.7)", "rgba(55, 55, 64, 0.3)"],
  tableText: ["rgba(0,0,0,0.7)", "rgba(255,255,255,0.7)"],
  desktopFilterButtonBorder: [
    "2px solid rgba(55, 55, 64, 0.7)",
    "2px solid white",
  ],
  desktopFilterSectionBg: ["white", "rgb(55, 55, 64)"],
  desktopSelectedFilterBg: ["#019da7", "#017a82"],
  toggleButtonBg: ["white", "rgba(0,0,0,0.1)"],
  toggleButtonText: ["rgba(0,0,0,0.7)", "rgba(255,255,255,0.5)"],
  filterApplyButtonBorder: ["1px solid rgba(0,0,0,0.7)", "1px solid white"],
  filterApplyButtonText: ["rgba(0,0,0,0.7)", "rgba(255,255,255,0.8)"],
  disabledButtonBorder: [
    "1px solid rgba(0,0,0,0.3)",
    "1px solid rgba(255,255,255,0.3)",
  ],
  disabledButtonText: ["rgba(0,0,0,0.4)", "rgba(255,255,255,0.4)"],
}

let joinPage = {
  background: ["#e8ecef", colors.grey[800]],
  formDefault: ["white", "transparent"],
  formSmAbove: ["white", colors.grey[600]],
  text: ["black", "rgba(255,255,255,0.9)"],
  submit: ["#01aeba", "#545363"],
  submitHover: ["#01c4d4", "#7a7986"],
  divider: ["rgba(0,0,0,0.4)", "rgba(255,255,255,0.8)"],
  dividerText: ["rgba(0,0,0,0.7)", "rgba(255,255,255,0.8)"],
  dividerTextBg: ["white", colors.grey[800]],
  inputBoxBorder: [
    "1px solid rgba(0,0,0,0.3)",
    "1px solid rgba(255,255,255,0.8)",
  ],
}

let homePage = {
  background: ["white", colors.grey[800]],
  text: ["rgba(0,0,0,0.54)", "rgba(255,255,255,0.9)"],
}

const componentThemes = {
  folderLayout,
  settings,
  path,
  sideBar,
  customButton,
  fileInfo,
  folderContents,
  notification,
  darkModeToggle,
  search,
}

const pageThemes = {
  joinPage,
  homePage,
}

const oneTheme = combineThemes({ ...componentThemes, ...pageThemes })

export default oneTheme
