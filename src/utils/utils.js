import { nanoid } from "nanoid"
import qs from "query-string"
import { SHORT_LINK_LENGTH } from "./constants"

// @helper - case insensitive sorting
function sortByName(list) {
  return list.sort((itemA, itemB) => {
    let nameA = String(itemA.name).toLowerCase()
    let nameB = String(itemB.name).toLowerCase()
    return nameA.localeCompare(nameB)
  })
}

export function getObjectKeysLength(object) {
  return Object.keys(object).length
}

export function convertToFolderMeta(object) {
  let ids = Object.keys(object)
  return sortByName(ids.map(id => ({ id: id, name: object[id] })))
}

export function roundOff(value) {
  return Math.round((value + Number.EPSILON) * 100) / 100
}

export function simplifyFileSize(byteSize) {
  let metric = ["B", "KB", "MB"]
  let index = 0

  while (byteSize >= 1024) {
    byteSize = byteSize / 1024
    index += 1
  }

  return `${roundOff(byteSize)} ${metric[index]}`
}

export function getShortLink() {
  return nanoid(SHORT_LINK_LENGTH)
}

export function getFileExtension(filename) {
  let dotPosition = filename.lastIndexOf(".")

  if (filename === "" || dotPosition < 1) {
    return ""
  }

  return filename.slice(dotPosition + 1)
}

export function dayStart(dateTime, toUnix = false) {
  let obj = new Date(dateTime)
  obj.setHours(0, 0, 0, 0)

  if (toUnix) {
    return obj.getTime()
  }

  return obj.toString()
}

export function stripToken(downloadUrl) {
  let url = new URL(downloadUrl)
  let bareUrl = url.origin + url.pathname
  let params = qs.parse(url.search)
  delete params.token
  let newParams = new URLSearchParams(params).toString()
  console.log("new url => ", `${bareUrl}?${newParams}`)
  return `${bareUrl}?${newParams}`
}
