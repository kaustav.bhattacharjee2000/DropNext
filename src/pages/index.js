import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import Typography from "@material-ui/core/Typography"
import Grid from "@material-ui/core/Grid"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import ListItemIcon from "@material-ui/core/ListItemIcon"
import ScheduleRoundedIcon from "@material-ui/icons/ScheduleRounded"
import CloudDoneRoundedIcon from "@material-ui/icons/CloudDoneRounded"
import ShareRoundedIcon from "@material-ui/icons/ShareRounded"
import CssBaseline from "@material-ui/core/CssBaseline"
import Box from "@material-ui/core/Box"
import { nanoid } from "nanoid"

import PageLayout from "../components/PageLayout"
import NavBar from "../components/NavBar"
import oneTheme from "../utils/theme"
import DemoImage from "../../assets/demo.png"

const useStyles = makeStyles(theme => ({
  root: {
    margin: 0,
    backgroundColor: oneTheme.homePage.background[theme.props.mode],
    width: "100vw",
    height: "100vh",
    display: "flex",
    flexFlow: "column",
    alignItems: "center",
    padding: theme.spacing(2),
    overflow: "auto",
    [theme.breakpoints.up("sm")]: {
      padding: theme.spacing(2, 4),
    },
  },
  paper: {
    width: "100%",
    height: "100%",
    display: "flex",
    flexFlow: "column",
    justifyContent: "center",
    color: oneTheme.homePage.text[theme.props.mode],
    textAlign: "center",
    [theme.breakpoints.up("sm")]: {
      textAlign: "auto",
      padding: theme.spacing(0, 4),
    },
  },
  grid: {
    flexGrow: 1,
    marginTop: theme.spacing(4),
    [theme.breakpoints.up("sm")]: {
      flex: 1,
      marginTop: 0,
    },
  },
  largeText: {
    color: "inherit",
    margin: theme.spacing(2, 0),
    fontSize: 28,
    [theme.breakpoints.up("sm")]: {
      fontSize: 30,
    },
    [theme.breakpoints.up("md")]: {
      fontSize: 36,
    },
  },
  blueText: {
    color: "#01aeba",
    fontSize: "inherit",
  },
  mediumText: {
    color: "inherit",
    fontSize: 15,
    [theme.breakpoints.up("sm")]: {
      fontSize: 16,
    },
    [theme.breakpoints.up("md")]: {
      fontSize: 18,
    },
  },
  benifitsContainer: {
    width: "100%",
    marginTop: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      maxWidth: 360,
    },
  },
  listItemIcon: {
    minWidth: "auto",
    marginRight: theme.spacing(2),
  },
  benifitItem: {
    marginTop: theme.spacing(1),
    [theme.breakpoints.up("sm")]: {
      paddingLeft: 0,
    },
  },
  listItemTitle: {
    fontSize: 16,
    fontWeight: "bold",
  },
  listItemDescription: {},
  icon: {
    width: 28,
    height: 28,
    color: "#01aeba",
  },
  demoImage: {
    width: "100%",
    height: "auto",
  },
}))

const benefits = [
  {
    title: "Free to use",
    description: "No charges for usage",
    icon: CloudDoneRoundedIcon,
  },
  {
    title: "Easy sharing",
    description: "Make any number of files as public and get their short links",
    icon: ShareRoundedIcon,
  },
  {
    title: "Expiration Date",
    description: "Equip your public files with a time-limited access",
    icon: ScheduleRoundedIcon,
  },
]

const PageContent = props => {
  const classes = useStyles({ wow: true })

  return (
    <Box className={classes.root}>
      <NavBar />
      <Grid container className={classes.grid} direction={"row"}>
        <Box clone order={{ xs: 2, sm: 1 }}>
          <Grid item xs={12} sm={6} md={4}>
            <Box className={classes.paper}>
              {/* Dialogue*/}
              <Typography component={"span"} className={classes.largeText}>
                Manage all your files
                <Typography className={classes.blueText}>
                  the easy way
                </Typography>
              </Typography>

              {/* Sub-dialogues */}
              <Typography className={classes.mediumText}>
                A comfortable way to have access to all your files.
              </Typography>

              {/* Benifits */}
              <List dense className={classes.benifitsContainer}>
                {benefits.map(({ title, description, icon: Icon }) => (
                  <ListItem key={nanoid()} className={classes.benifitItem}>
                    <ListItemIcon classes={{ root: classes.listItemIcon }}>
                      <Icon className={classes.icon} />
                    </ListItemIcon>
                    <ListItemText
                      primary={
                        <Typography className={classes.listItemTitle}>
                          {title}
                        </Typography>
                      }
                      secondary={
                        <Typography className={classes.listItemDescription}>
                          {description}
                        </Typography>
                      }
                    />
                  </ListItem>
                ))}
              </List>
            </Box>
          </Grid>
        </Box>

        <Box clone order={{ xs: 1, sm: 2 }}>
          {/* Demo image */}
          <Grid item xs={12} sm={6} md={8}>
            <Box className={classes.paper}>
              <img
                src={DemoImage}
                alt="DropNext preview - desktop & mobile"
                className={classes.demoImage}
              />
            </Box>
          </Grid>
        </Box>
      </Grid>
    </Box>
  )
}

const IndexPage = props => {
  return (
    <PageLayout title="Home">
      <CssBaseline />
      <PageContent />
    </PageLayout>
  )
}

export default IndexPage
