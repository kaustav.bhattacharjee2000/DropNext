import React from "react"
import { navigate } from "gatsby"
import { makeStyles } from "@material-ui/core/styles"
import Typography from "@material-ui/core/Typography"
import Avatar from "@material-ui/core/Avatar"
import LinkIcon from "@material-ui/icons/Link"
import CircularProgress from "@material-ui/core/CircularProgress"
import Box from "@material-ui/core/Box"
import { useQuery } from "react-query"

import PageLayout from "../../components/PageLayout"
import { simplifyFileSize } from "../../utils/utils"
import { useAuth } from "../../contexts/auth-context"
import { useNotification } from "../../contexts/notification-context"
import * as LinkAPI from "../../api/shortLink"
import NavBar from "../../components/NavBar"
import oneTheme from "../../utils/theme"
import { stripToken } from "../../utils/utils"

const useStyles = makeStyles(theme => ({
  root: {
    width: "100vw",
    height: "100vh",
    backgroundColor: oneTheme.joinPage.background[theme.props.mode],
    display: "flex",
    flexFlow: "column",
    color: oneTheme.joinPage.text[theme.props.mode],
  },
  body: {
    flexGrow: 1,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  content: {
    width: "100%",
    margin: theme.spacing(0, 2),
    [theme.breakpoints.up("sm")]: {
      maxWidth: 450,
      margin: 0,
    },
  },
  list: {
    backgroundColor: oneTheme.joinPage.formSmAbove[theme.props.mode],
    padding: theme.spacing(2, 3),
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "column",
    borderRadius: theme.spacing(1),
  },
  listItem: {
    padding: theme.spacing(1),
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  owner: {
    backgroundColor: oneTheme.joinPage.formSmAbove[theme.props.mode],
    display: "flex",
    alignItems: "center",
    columnGap: theme.spacing(2),
    marginBottom: theme.spacing(2),
    width: "auto",
    borderRadius: theme.spacing(1),
    padding: theme.spacing(1, 2),
  },
  fileActions: {
    width: "100%",
    display: "flex",
    padding: theme.spacing(1),
    columnGap: theme.spacing(2),
  },
  blueButton: {
    padding: theme.spacing(0.75, 2),
    borderRadius: 5,
    backgroundColor: "#018b95",
    color: "white",
    cursor: "pointer",
  },
  disabledButton: {
    flex: 1,
    borderRadius: 5,
    padding: theme.spacing(0.75, 2),
    border: oneTheme.joinPage.inputBoxBorder[theme.props.mode],
    textAlign: "center",
    cursor: "pointer",
  },
  bold: {
    fontWeight: "bold",
  },
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  sectionTitle: {
    fontWeight: "bold",
    fontSize: 18,
  },
  link: {
    color: "#01aeba",
    cursor: "pointer",
    transform: "rotate(-45deg)",
  },
  navbar: {
    padding: theme.spacing(1),
  },
  progress: {
    color: "#01aeba",
    borderRadius: 10,
    strokeLinecap: "round",
  },
}))

function SharedFileDisplay(props) {
  const classes = useStyles()
  const { showNotification } = useNotification()
  const { firebase } = useAuth()
  const linkId = props.params.linkId

  const [gotFirebase, setGotFirebase] = React.useState(false)

  const handleAddToDrive = () => {
    showNotification({
      type: "warn",
      message: "Feature is not implemented yet",
    })
  }

  const handleCopyLink = () => {
    if (typeof window && window && window.navigator) {
      navigator.clipboard.writeText(window.location.href)
      showNotification({ type: "success", message: "Link copied to clipboard" })
    }
  }

  const objectToDate = obj => {
    let unixTime = obj.seconds * 1000
    return new Date(unixTime).toLocaleDateString()
  }

  const { data, status } = useQuery(
    [linkId, linkId],
    ({ queryKey }) => LinkAPI.getLinkData(firebase, queryKey[1]),
    {
      cacheTime: Infinity,
      refetchOnWindowFocus: false,
      staleTime: Infinity,
      retry: false,
      onSuccess: data => {
        if (!data) {
          console.error("Data is undefined")
          navigate("/404")
        }
      },
      onError: error => {
        console.error("Error while fetching short link data ==> ", error)
        navigate("/404")
      },
    }
  )

  console.log("DATA => ", data)
  console.log("STATUS => ", status)

  if (firebase && !gotFirebase) {
    setGotFirebase(true)
  }

  return (
    <Box className={classes.root}>
      <Box className={classes.navbar}>
        <NavBar />
      </Box>

      <Box className={classes.body}>
        {!data || status === "loading" ? (
          <CircularProgress
            size={"8rem"}
            thickness={3}
            classes={{ circle: classes.progress }}
          />
        ) : (
          <Box className={classes.content}>
            {/* User details */}
            <Box className={classes.owner}>
              <Avatar
                alt="owner avatar"
                src={data.owner.avatar}
                className={classes.avatar}
              />
              <Typography component="span">
                <Typography
                  display="inline"
                  className={classes.bold}
                >{`@${data.owner.displayName}`}</Typography>
                {` wants to share a file with you`}
              </Typography>
            </Box>

            {/* File details */}
            <Box className={classes.list}>
              {/* Section title with link */}
              <Box className={classes.listItem}>
                <Typography className={classes.sectionTitle}>
                  File Details
                </Typography>
                <LinkIcon className={classes.link} onClick={handleCopyLink} />
              </Box>

              {/* Name */}
              <Box className={classes.listItem}>
                <Typography>Name</Typography>
                <Typography>{data.name}</Typography>
              </Box>

              {/* Size */}
              <Box className={classes.listItem}>
                <Typography>Size</Typography>
                <Typography>{simplifyFileSize(data.size)}</Typography>
              </Box>

              {/* Type */}
              {/* <Box className={classes.listItem}>
              <Typography>Type</Typography>
              <Typography>{data.type}</Typography>
            </Box> */}

              {/* Created at */}
              <Box className={classes.listItem}>
                <Typography>Created On</Typography>
                <Typography>{objectToDate(data.createdAt)}</Typography>
              </Box>

              {/* Modified at */}
              <Box className={classes.listItem}>
                <Typography>Modified On</Typography>
                <Typography>{objectToDate(data.modifiedAt)}</Typography>
              </Box>

              {/* File actions */}
              <Box className={classes.fileActions}>
                <a
                  href={stripToken(data.downloadUrl)}
                  style={{ textDecoration: "none" }}
                  target="_blank"
                  rel="noreferrer"
                >
                  <Box className={classes.blueButton}>
                    <Typography>View File</Typography>
                  </Box>
                </a>
                <Box
                  className={classes.disabledButton}
                  onClick={handleAddToDrive}
                >
                  <Typography>Add to Drive</Typography>
                </Box>
              </Box>
            </Box>
          </Box>
        )}
      </Box>
    </Box>
  )
}

function Shared(props) {
  return (
    <PageLayout title="Workspace" allowNotification>
      <SharedFileDisplay {...props} />
    </PageLayout>
  )
}

export default Shared
