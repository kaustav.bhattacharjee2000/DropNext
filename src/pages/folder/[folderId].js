import React from "react"
import { navigate } from "gatsby"

import FolderLayout from "../../components/FolderLayout"
import PageLayout from "../../components/PageLayout"
import { useAuth } from "../../contexts/auth-context"

function PrivateRoute({ children, next, search }) {
  const { user } = useAuth()

  // Fix from https://github.com/gatsbyjs/gatsby/issues/309#issuecomment-347595352
  const windowGlobal = typeof window !== "undefined" && window

  let nextPath = next
  if (search) {
    nextPath += search
  }

  // if not authenticated, redirect to /join with "next" param
  if (!user) {
    if (windowGlobal) {
      navigate(`/join/?next=${nextPath}`)
    } else {
      console.error(
        "Are you running this in browser ? Doesn't seem to me like you do..."
      )
    }
    return null
  }

  // otherwise show the folder's contents
  return children
}

function Workspace(props) {
  console.log("query => ", props.location.search)

  return (
    <PageLayout title="Workspace" allowNotification>
      <PrivateRoute
        next={props.location.pathname}
        search={props.location.search}
      >
        <FolderLayout
          folderId={props.params.folderId}
          search={props.location.search}
        />
      </PrivateRoute>
    </PageLayout>
  )
}

export default Workspace
