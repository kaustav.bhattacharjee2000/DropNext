import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import Typography from "@material-ui/core/Typography"
import CssBaseline from "@material-ui/core/CssBaseline"
import Box from "@material-ui/core/Box"
import { navigate } from "gatsby"

import PageLayout from "../components/PageLayout"
import oneTheme from "../utils/theme"
import BrokenFileImage from "../../assets/brokenfile.png"

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: oneTheme.homePage.background[theme.props.mode],
    width: "100vw",
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(4),
    color: oneTheme.homePage.text[theme.props.mode],
  },
  wrapper: {
    maxWidth: 360,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlign: "center",
  },
  title: {
    color: "#e84545",
    fontSize: 32,
    margin: theme.spacing(1, 0, 2, 0),
  },
  content: {
    fontSize: 16,
  },
  redirect: {
    marginTop: theme.spacing(2),
    width: "max-content",
    padding: theme.spacing(1, 1.5),
    borderRadius: theme.spacing(0.5),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    backgroundColor: "#017a82",
    color: "white",
    cursor: "pointer",
  },
  image: {
    height: theme.spacing(16),
    width: theme.spacing(16),
  },
}))

function FunnyContent(props) {
  const classes = useStyles()

  return (
    <Box className={classes.root}>
      <Box className={classes.wrapper}>
        <img
          src={BrokenFileImage}
          alt="broken file"
          className={classes.image}
        />
        <Typography className={classes.title}>404 Not Found</Typography>
        <Typography className={classes.content}>
          We can't seem to find the page you're looking for. Either the url is
          invalid or the page doesn't exist.
        </Typography>
        <Box className={classes.redirect} onClick={() => navigate("/")}>
          Back to Home
        </Box>
      </Box>
    </Box>
  )
}

function NotFoundPage(props) {
  return (
    <PageLayout title="404">
      <CssBaseline />
      <FunnyContent />
    </PageLayout>
  )
}

export default NotFoundPage
