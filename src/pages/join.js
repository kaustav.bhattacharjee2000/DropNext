import React, { Fragment, useState, useRef } from "react"
import { navigate } from "gatsby"
import Box from "@material-ui/core/Box"
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Container from "@material-ui/core/Container"
import Divider from "@material-ui/core/Divider"
import CircularProgress from "@material-ui/core/CircularProgress"
import { makeStyles } from "@material-ui/core/styles"
import queryString from "query-string"
import { Link } from "gatsby"

// import GoogleLogo from "../svg/google.svg"
import GoogleLogo from "../../assets/google.inline.svg"
import PageLayout from "../components/PageLayout"
import AppName from "../components/AppName"
import oneTheme from "../utils/theme"
import { useAuth } from "../contexts/auth-context"
import { useNotification } from "../contexts/notification-context"
import {
  FORM_WIDTH,
  MAX_DISPLAY_NAME_LENGTH,
  MAX_EMAIL_LENGTH,
  MAX_PASSWORD_LENGTH,
  ALLOWED_DISPLAY_NAME_CHARS,
  ALLOWED_EMAIL_CHARS,
  ALLOWED_PASSWORD_CHARS,
} from "../utils/constants"

const useStyles = makeStyles(theme => ({
  root: {
    color: oneTheme.joinPage.text[theme.props.mode],
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontFamily: "Nunito Sans",
    backgroundColor: oneTheme.joinPage.background[theme.props.mode],
  },
  formWrapper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
    borderRadius: 5,
    backgroundColor: oneTheme.joinPage.formDefault[theme.props.mode],
    [theme.breakpoints.up("sm")]: {
      height: "auto",
      width: FORM_WIDTH,
      padding: theme.spacing(4, 4, 2),
      boxShadow: "0 4px 30px -8px rgba(0,0,0,0.4)",
      backgroundColor: oneTheme.joinPage.formSmAbove[theme.props.mode],
    },
  },
  form: {
    width: "100%",
  },
  submit: {
    width: "100%",
    margin: theme.spacing(3, 0, 2),
    fontWeight: "bold",
    padding: theme.spacing(1, 0),
    backgroundColor: oneTheme.joinPage.submit[theme.props.mode],
    "&:hover": {
      backgroundColor: oneTheme.joinPage.submitHover[theme.props.mode],
    },
  },
  textBoxContainer: {
    width: "100%",
  },
  textBoxInput: {
    width: "100%",
    boxSizing: "border-box",
    backgroundColor: "transparent",
    padding: theme.spacing(1.25, 1.5),
    outline: "none",
    borderRadius: 3,
    border: oneTheme.joinPage.inputBoxBorder[theme.props.mode],
    borderWidth: 1,
    fontSize: 16,
    fontFamily: "Nunito Sans",
    color: oneTheme.joinPage.text[theme.props.mode],
  },
  customLoginButton: {
    boxSizing: "border-box",
    width: "100%",
    padding: theme.spacing(0.25),
    borderRadius: 100,
    cursor: "pointer",
    color: "white",
    background: "linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)",
  },
  customLoginButtonInner: {
    boxSizing: "border-box",
    width: "100%",
    padding: theme.spacing(0.75),
    borderRadius: 100,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    columnGap: theme.spacing(2),
    color: "inherit",
  },
  googleLogo: {
    width: 24,
    height: 24,
    backgroundColor: "white",
    padding: theme.spacing(0.5),
    borderRadius: 100,
  },
  sep: {
    width: "100%",
    position: "relative",
    margin: theme.spacing(4, 0),
    backgroundColor: oneTheme.joinPage.divider[theme.props.mode],
  },
  sepDivider: {
    height: 2,
  },
  sepTextContainer: {
    position: "absolute",
    width: "100%",
    display: "flex",
    justifyContent: "center",
  },
  sepText: {
    color: oneTheme.joinPage.dividerText[theme.props.mode],
    fontWeight: 700,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: oneTheme.joinPage.dividerTextBg[theme.props.mode],
    [theme.breakpoints.up("sm")]: {
      backgroundColor: oneTheme.joinPage.formSmAbove[theme.props.mode],
    },
  },
  appname: {
    boxSizing: "border-box",
    width: "60%",
    marginBottom: theme.spacing(3),
    height: "auto",
  },
  footerBox: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    columnGap: theme.spacing(1),
    marginTop: theme.spacing(1),
  },
  textButton: {
    cursor: "pointer",
    fontWeight: "bold",
  },
  secretTextBoxContainer: {
    display: "flex",
    width: "100%",
    borderRadius: 3,
    border: oneTheme.joinPage.inputBoxBorder[theme.props.mode],
    boxSizing: "border-box",
    padding: theme.spacing(1.25, 1.5),
    color: oneTheme.joinPage.text[theme.props.mode],
  },
  secretTextBoxInput: {
    backgroundColor: "transparent",
    outline: "none",
    border: 0,
    fontSize: 16,
    fontFamily: "Nunito Sans",
    flex: 1,
    color: "inherit",
  },
  secretButton: {
    marginLeft: theme.spacing(1),
    cursor: "pointer",
  },
  progress: {
    color: "#01aeba",
    borderRadius: 10,
    strokeLinecap: "round",
  },
}))

function CustomLoginButton({ login, ...restProps }) {
  const classes = useStyles()

  return (
    <Box className={classes.customLoginButton} {...restProps}>
      <Box className={classes.customLoginButtonInner} {...restProps}>
        <GoogleLogo className={classes.googleLogo} />
        <Typography style={{ fontWeight: "bolder" }}>{`${
          login ? "Log in" : "Sign up"
        } with Google`}</Typography>
      </Box>
    </Box>
  )
}

function Separator() {
  const classes = useStyles()

  return (
    <Box className={classes.sep}>
      <Divider className={classes.sepDivider} />
      <Box mt={-1.5} className={classes.sepTextContainer}>
        <Typography className={classes.sepText}>OR</Typography>
      </Box>
    </Box>
  )
}

function Form(props) {
  const classes = useStyles()
  const { loading, user, login, signup, googleAuth } = useAuth()
  const { showNotification } = useNotification()
  const [displayLoginPage, setDisplayLoginPage] = useState(true)
  const [showPassword, setShowPassword] = useState(false)
  const [submitting, setSubmitting] = useState(false)

  const nameRef = useRef()
  const emailRef = useRef()
  const passwordRef = useRef()

  if (loading) {
    return (
      <Box className={classes.root}>
        <CircularProgress
          size={"8rem"}
          thickness={3}
          classes={{ circle: classes.progress }}
        />
      </Box>
    )
  }

  if (user) {
    const { next } = queryString.parse(props.location.search)

    if (next) {
      navigate(next)
      return null
    }

    navigate("/")
    return null
  }

  const toggleDisplayPage = () => {
    setDisplayLoginPage(prev => !prev)
  }

  const toggleShowPassword = () => {
    setShowPassword(prev => !prev)
  }

  const onSignInWithGoogle = async event => {
    console.log("pressed")

    try {
      await googleAuth()
    } catch (error) {
      console.error("Error at join.js => ", error)
      showNotification({ type: "error", message: error.message })
    }
  }

  const validateDetails = (displayName, email, password) => {
    let errorMessage = null

    // validate displayname if exists
    if (displayName) {
      if (displayName.length === 0) {
        errorMessage = `Display name cannot be empty`
      }
      if (displayName.length > MAX_DISPLAY_NAME_LENGTH) {
        errorMessage = `Display name can be at max ${MAX_DISPLAY_NAME_LENGTH} characters`
      }
      if (ALLOWED_DISPLAY_NAME_CHARS.test(displayName)) {
        errorMessage = "Only alphabets, numbers, _ is allowed in display name"
      }
    }

    // validate email
    if (email.length > MAX_EMAIL_LENGTH || ALLOWED_EMAIL_CHARS.test(email)) {
      errorMessage = `Invalid email address`
    }

    // validate password
    if (password.length > MAX_PASSWORD_LENGTH) {
      errorMessage = `Password can be at max ${MAX_PASSWORD_LENGTH} characters`
    }

    if (ALLOWED_PASSWORD_CHARS.test(password)) {
      errorMessage = "Only alphabets, numbers, _ is allowed in password"
    }

    return errorMessage
  }

  const onSubmit = async event => {
    event.preventDefault()

    let displayName = nameRef.current && nameRef.current.value
    let email = emailRef.current && emailRef.current.value
    let password = passwordRef.current && passwordRef.current.value

    let errorMessage = validateDetails(displayName, email, password)
    if (errorMessage) {
      showNotification({ type: "warn", message: errorMessage })
      return null
    }

    try {
      setSubmitting(true)

      if (displayLoginPage) {
        await login(email, password)
      } else {
        await signup(displayName, email, password)
      }
    } catch (error) {
      console.error(error)
      showNotification({ type: "error", message: error.message })
    } finally {
      setSubmitting(false)
    }
  }

  return (
    <main className={classes.root}>
      <Container className={classes.formWrapper}>
        <Box align="center">
          <Link to="/">
            <AppName fill="#01c4d4" className={classes.appname} />
          </Link>
        </Box>

        <CustomLoginButton
          login={displayLoginPage}
          onClick={onSignInWithGoogle}
        />

        <Separator />

        <form className={classes.form} noValidate onSubmit={onSubmit}>
          <Grid container spacing={1}>
            {!displayLoginPage && (
              <Grid item xs={12}>
                <Box className={classes.textBoxContainer}>
                  <input
                    ref={nameRef}
                    className={classes.textBoxInput}
                    placeholder="Display Name"
                    name="name"
                  />
                </Box>
              </Grid>
            )}

            <Grid item xs={12}>
              <Box className={classes.textBoxContainer}>
                <input
                  ref={emailRef}
                  className={classes.textBoxInput}
                  placeholder="Email Address"
                  name="email"
                />
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Box className={classes.secretTextBoxContainer}>
                <input
                  className={classes.secretTextBoxInput}
                  type={showPassword ? "text" : "password"}
                  ref={passwordRef}
                  placeholder="Password"
                  name="password"
                />
                <Typography
                  className={classes.secretButton}
                  onClick={toggleShowPassword}
                >
                  {showPassword ? "HIDE" : "SHOW"}
                </Typography>
              </Box>
            </Grid>
          </Grid>

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            <Typography>{displayLoginPage ? "Log In" : "Sign Up"}</Typography>
            {submitting && (
              <CircularProgress
                color="inherit"
                size="1.5rem"
                style={{ marginLeft: "10px" }}
              />
            )}
          </Button>

          {displayLoginPage && (
            <Box className={classes.footerBox}>
              <Typography className={classes.textButton}>
                Forgot password?
              </Typography>
            </Box>
          )}

          <Box className={classes.footerBox}>
            {displayLoginPage ? (
              <Fragment>
                <Typography>{"Don't have an account? "}</Typography>
                <Typography
                  className={classes.textButton}
                  onClick={toggleDisplayPage}
                >
                  Sign up
                </Typography>
              </Fragment>
            ) : (
              <Fragment>
                <Typography>{"Already have an account? "}</Typography>
                <Typography
                  className={classes.textButton}
                  onClick={toggleDisplayPage}
                >
                  Log in
                </Typography>
              </Fragment>
            )}
          </Box>
        </form>
      </Container>
    </main>
  )
}

function JoinPage(props) {
  return (
    <PageLayout title="Join" allowNotification>
      <Form {...props} />
    </PageLayout>
  )
}

export default JoinPage
