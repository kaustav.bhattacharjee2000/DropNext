import { getDatabase, getStorage } from "../utils/firebase"
import { getShortLink, getFileExtension } from "../utils/utils"
import { nanoid } from "nanoid"

/**
 * Fetches a folder by id
 *
 * Firestore :
 * 1) /folders/{folder_id}
 *
 * @param {any}    firebase  firebase instance
 * @param {string} folderId
 * @returns object if folder exists otherwise null
 */
export async function getFolder(firebase, folderId = null) {
  if (!firebase) {
    console.log("no firebase !")
    return
  }

  console.log("got firebase !!!")

  const { folders } = getDatabase()
  const folderRef = await folders.doc(folderId).get()

  console.log("folderRef exists => ", folderRef.exists)

  if (folderRef.exists) {
    console.log("data => ", { id: folderId, ...folderRef.data() })
    return { id: folderId, ...folderRef.data() }
  }
  return null
}

/**
 * Creates a new folder
 *
 * Firestore :
 * 1) /folders
 * 2) /folders/{folder_id}
 *
 * Realtime :
 * 1) /search/{user_id}/{folder_id}
 *
 * @param {any}     firebase          firebase instance
 * @param {object}  folderObject      object with folder's data
 * @param {string}  parentFolderId
 * @returns created folder object
 */
export async function createFolder(firebase, folderObject, parentFolderId) {
  if (!firebase || !folderObject || !parentFolderId) {
    return
  }

  const { folders, getCurrentTimestamp, realtime } = getDatabase()

  const folderStructure = {
    ...folderObject,
    createdAt: getCurrentTimestamp(),
    modifiedAt: getCurrentTimestamp(),
  }

  // create the folder first
  const createdFolderRef = await folders.add(folderStructure)
  console.log("created folder with id : ", createdFolderRef.id)

  // make an entry in parent folder
  let updation = {}
  updation[`folders.${createdFolderRef.id}`] = folderObject.name

  await folders.doc(parentFolderId).update(updation)
  console.log("updated parent folder !")

  // Add an entry in search in realtime db
  await realtime
    .ref(`search/${folderObject.owner}/${createdFolderRef.id}`)
    .set({
      name: folderObject.name,
      root: folderObject.path[0].name,
      type: "folder",
      lower: String(folderObject.name).toLowerCase(),
    })

  return null
}

/**
 * Renames a folder
 *
 * Firestore :
 * 1) /folders/{folder_id}
 *
 * @param {any}     firebase        firebase instance
 * @param {object}  folderObject    object with folder data
 * @param {string}  newName         folder's new name
 * @param {string}  parentFolderId
 */
export async function renameFolder(
  firebase,
  folderObject,
  newName,
  parentFolderId
) {
  if (!firebase || !folderObject || !newName || !parentFolderId) {
    return
  }

  const { folders, getCurrentTimestamp, realtime } = getDatabase()
  let folderId = folderObject.id
  let owner = folderObject.owner

  await folders
    .doc(folderId)
    .update({ name: newName, modifiedAt: getCurrentTimestamp() })
  console.log("updated folder's name !")

  let parentUpdation = {}
  parentUpdation[`folders.${folderId}`] = newName

  await folders.doc(parentFolderId).update(parentUpdation)
  console.log("updated parent folder !")

  // Update search in realtime db
  await realtime.ref(`search/${owner}/${folderId}`).update({
    name: newName,
    lower: String(newName).toLowerCase(),
  })

  return null
}

/**
 * Triggered when a folder is deleted from drive
 *
 * Firestore :
 * 1) /folders/{folder_id}
 *
 * Realtime:
 * 1) /search/{user_id}/{folder_id}
 *
 * @param {any}     firebase         firebase instance
 * @param {object}  folderObject     object with folder data
 * @param {string}  parentFolderId
 * @param {string}  trashFolderId
 */
export async function moveFolderToTrash(
  firebase,
  folderObject,
  parentFolderId,
  trashFolderId
) {
  if (!firebase) {
    return
  }

  const { folders, fieldValue, realtime, firestore } = getDatabase()
  let folderId = folderObject.id
  let ownerId = folderObject.owner

  let firestoreBatch = firestore.batch()
  let trashFolderRef = folders.doc(trashFolderId)
  let parentFolderRef = folders.doc(parentFolderId)
  let deletedFolderRef = folders.doc(folderId)

  // Update trash folder and parent folder and the deleted folder itself
  firestoreBatch.update(deletedFolderRef, "path", [
    { id: trashFolderId, name: "trash" },
  ])
  firestoreBatch.update(
    trashFolderRef,
    `folders.${folderId}`,
    folderObject.name
  )
  firestoreBatch.update(
    parentFolderRef,
    `folders.${folderId}`,
    fieldValue.delete()
  )
  await firestoreBatch.commit()

  console.log("Folder Part 1 done -------")

  // Update entry in search in realtime db
  await realtime.ref(`search/${ownerId}/${folderId}`).update({
    root: "trash",
  })

  console.log("Folder Part 2 done -------")

  return null
}

/**
 * Triggered when a folder is deleted from trash folder
 *
 * Firestore :
 * 1) /folders/{folder_id}
 *
 * Realtime :
 * 1) /search/{user_id}/{folder_id}
 *
 * @param {any}     firebase        firebase instance
 * @param {object}  folderObject    object with folder data
 * @param {string}  parentFolderId
 */
export async function permanentlyDeleteFolder(
  firebase,
  folderObject,
  parentFolderId
) {
  if (!firebase) {
    return
  }

  const { folders, fieldValue, realtime } = getDatabase()
  let folderId = folderObject.id
  let ownerId = folderObject.owner

  let parentUpdation = {}
  parentUpdation[`folders.${folderId}`] = fieldValue.delete()

  await folders.doc(parentFolderId).update(parentUpdation)
  console.log("updated parent folder !")

  await folders.doc(folderId).delete()
  console.log("deleted folder !")

  // Delete entry from search in realtime db
  await realtime.ref(`search/${ownerId}/${folderId}`).remove()

  return null
}

/**
 * Triggered when upoading a file
 *
 * Firestore :
 * 1) /files
 * 2) /folders/{folder_id}
 *
 * Realtime :
 * 1) /search/{user_id}/{file_id}
 *
 * Storage :
 * 1) /{user_id}
 *
 * @param {any}     firebase        firebase instance
 * @param {object}  fileObject      object with folder data
 * @param {string}  parentFolderId
 */
export async function uploadFile(firebase, fileObject, parentFolderId) {
  if (!firebase || !fileObject || !parentFolderId) {
    return
  }

  const storage = getStorage()
  const { folders, files, getCurrentTimestamp, realtime } = getDatabase()

  const storageRef = storage.ref()
  const { file, owner, ...restFileDetails } = fileObject
  console.log("fileObject")
  const storageFilePath = `${owner}/${nanoid()}`

  const fileRef = storageRef.child(storageFilePath)
  console.log("File stored at : ", storageFilePath)
  console.log("File ref => ", fileRef)

  const storedFileMeta = {
    customMetadata: {
      owner: owner,
      visibility: restFileDetails.visibility,
    },
  }

  // Cannot determine progress using async/await
  // Use .on to listen to changes
  const uploadedFileSnapshot = await fileRef.put(file, storedFileMeta)
  console.log("Successfully uploaded file !")
  console.log("uploadedFileSnapshot => ", uploadedFileSnapshot)

  const downloadUrl = await uploadedFileSnapshot.ref.getDownloadURL()

  // Make entry in database
  const fileStructure = {
    ...restFileDetails,
    owner: owner,
    storagePath: storageFilePath,
    downloadUrl: downloadUrl,
    shortLink: "",
    fileType: file.type,
    fileExtension: getFileExtension(restFileDetails.name),
    createdAt: getCurrentTimestamp(),
    modifiedAt: getCurrentTimestamp(),
  }

  console.log("fileStructure => ", fileStructure)

  // create the file document first
  const createdFileRef = await files.add(fileStructure)
  console.log("created file document with id : ", createdFileRef.id)

  // make an entry in parent folder
  let updation = {}
  updation[`files.${createdFileRef.id}`] = fileObject.name

  await folders.doc(parentFolderId).update(updation)
  console.log("updated parent folder !")

  // Add an entry in search in realtime db
  await realtime.ref(`search/${owner}/${createdFileRef.id}`).set({
    name: restFileDetails.name,
    root: restFileDetails.path[0].name,
    type: "file",
    fileType: file.type,
    fileExtension: getFileExtension(restFileDetails.name),
    lower: String(restFileDetails.name).toLowerCase(),
    size: restFileDetails.size,
  })

  return null
}

/**
 * Fetches a file by id
 *
 * Firestore :
 * 1) /files/{file_id}
 *
 * @param {any}     firebase  firebase instance
 * @param {string}  fileId
 */
export async function getFile(firebase, fileId) {
  if (!firebase || !fileId) {
    return
  }

  const { files } = getDatabase()
  const fileRef = await files.doc(fileId).get()

  console.log("fileRef exists => ", fileRef.exists)

  if (fileRef.exists) {
    console.log("data => ", { id: fileId, ...fileRef.data() })
    return { id: fileId, ...fileRef.data() }
  }
  return null
}

/**
 * Update meta data of file
 *
 * Firestore :
 * 1) /files/{file_id}
 * 2) /folders/{folder_id}
 *
 * Realtime :
 * 1) /shortLinks/{link_id}
 * 2) /search/{user_id}/{file_id}
 *
 * Storage :
 * 1) /{user_id}/{storage_id}
 *
 * @param {any}     firebase        firebase instance
 * @param {string}  fileId
 * @param {object}  updateFileMeta  object for what to update
 * @param {object}  additionalData  object for existing file data
 * @param {string}  parentFolderId
 */
export async function updateFile(
  firebase,
  fileId,
  updateFileMeta,
  additionalData,
  parentFolderId
) {
  if (!firebase || !fileId || !parentFolderId || !updateFileMeta) {
    return
  }

  console.log("ENTERED -----")
  const { files, folders, getCurrentTimestamp, realtime } = getDatabase()
  let generatedShortLink = getShortLink()
  const storage = getStorage()
  const storageRef = storage.ref()

  console.log("got these additionalData ===> ", additionalData)

  if (updateFileMeta.visibility || updateFileMeta.sharableUntil) {
    // Make a short link if not exists otherwise update realtime db
    let fileShortLink = additionalData.file.shortLink

    // Save in db only what file data is necessary
    let { path, owner, storagePath, ...restFileData } = additionalData.file

    // Minal owner data to display
    let minimalOwner = {
      id: additionalData.owner.id,
      displayName: additionalData.owner.displayName,
      avatar: additionalData.owner.avatar,
    }

    let shortLinkStructure = {
      ...restFileData,
      visibility: updateFileMeta.visibility || "public",
      sharableUntil: updateFileMeta.sharableUntil,
      owner: minimalOwner,
    }

    // Already has a short link
    if (fileShortLink.length > 0) {
      await realtime.ref(`shortLinks/${fileShortLink}`).set(shortLinkStructure)
    }
    // Otherwise generate one (this case only applies once for each file)
    else {
      await realtime
        .ref(`shortLinks/${generatedShortLink}`)
        .set(shortLinkStructure)
    }

    await storageRef.child(additionalData.file.storagePath).updateMetadata({
      customMetadata: {
        owner: minimalOwner.id,
        visibility: updateFileMeta.visibility || "public",
        sharableUntil: updateFileMeta.sharableUntil,
      },
    })
  }

  // update file's document
  await files.doc(fileId).update({
    ...updateFileMeta,
    shortLink:
      additionalData.file.shortLink.length > 0
        ? additionalData.file.shortLink
        : generatedShortLink,
    modifiedAt: getCurrentTimestamp(),
  })
  console.log("updated file's meta !")

  // Only update parent folder & search in realtime db if filename changes
  if (updateFileMeta.name) {
    let parentUpdation = {}
    parentUpdation[`files.${fileId}`] = updateFileMeta.name

    await folders.doc(parentFolderId).update(parentUpdation)
    console.log("updated parent folder !")

    // Update search in realtime db
    await realtime.ref(`search/${additionalData.owner.id}/${fileId}`).update({
      name: updateFileMeta.name,
      lower: String(updateFileMeta.name).toLowerCase(),
    })
  }

  return null
}

/**
 * Triggered when file is deleted from drive
 *
 * Firestore :
 * 1) /folders/{folder_id}
 * 2) /files/{file_id}
 *
 * Realtime :
 * 1) /search/{user_id}/{file_id}
 *
 * @param {any}     firebase        firebase instance
 * @param {object}  fileObject      object containing current file's data
 * @param {string}  parentFolderId
 * @param {string}  trashFolderId
 */
export async function moveFileToTrash(
  firebase,
  fileObject,
  parentFolderId,
  trashFolderId
) {
  if (!firebase || !fileObject || !fileObject || !trashFolderId) {
    return
  }

  const { folders, files, fieldValue, realtime, firestore } = getDatabase()
  let fileId = fileObject.id
  let ownerId = fileObject.owner

  let firestoreBatch = firestore.batch()
  let trashFolderRef = folders.doc(trashFolderId)
  let parentFolderRef = folders.doc(parentFolderId)
  let deletedFileRef = files.doc(fileId)

  // Update trash folder and parent folder
  firestoreBatch.update(deletedFileRef, "path", [
    { id: trashFolderId, name: "trash" },
  ])
  firestoreBatch.update(trashFolderRef, `files.${fileId}`, fileObject.name)
  firestoreBatch.update(parentFolderRef, `files.${fileId}`, fieldValue.delete())
  await firestoreBatch.commit()

  console.log("File Part 1 done -------")

  // Update entry in search in realtime db
  await realtime.ref(`search/${ownerId}/${fileId}`).update({
    root: "trash",
  })

  console.log("File Part 2 done -------")

  return null
}

/**
 * Triggered when a file is deleted from trash folder.
 *
 * Firestore :
 * 1) /files/{file_id}
 * 2) /folders/{folder_id}
 *
 * Realtime :
 * 1) /search/{user_id}/{file_id}
 * 2) /shortLinks/{link_id}
 *
 * Storage :
 * 1) /{user_id}/{storage_id}
 *
 * @param {any}     firebase          firebase instance
 * @param {object}  fileObject        object with current file's data
 * @param {object}  fileStoragePath   firebase storage path for current file
 * @param {string}  parentFolderId
 */
export async function permanentlyDeleteFile(
  firebase,
  fileObject,
  fileStoragePath,
  parentFolderId
) {
  if (!firebase || !fileObject || !fileStoragePath || !parentFolderId) {
    return
  }

  const { files, folders, fieldValue, realtime } = getDatabase()
  const fileStorageRef = getStorage().ref().child(fileStoragePath)

  let fileId = fileObject.id
  let ownerId = fileObject.owner
  let shortLink = fileObject.shortLink

  let parentUpdation = {}
  parentUpdation[`files.${fileId}`] = fieldValue.delete()

  // Remove entry from parent folder
  await folders.doc(parentFolderId).update(parentUpdation)
  console.log("updated parent folder !")

  // Delete file document
  await files.doc(fileId).delete()
  console.log("deleted file document !")

  // Delete from realtime db if short link was created before
  if (shortLink) {
    await realtime.ref(`shortLinks/${shortLink}`).remove()
  }

  // Delete entry from search in realtime db
  await realtime.ref(`search/${ownerId}/${fileId}`).remove()

  // Delete file from storage
  await fileStorageRef.delete()
  console.log("deleted file from storage")

  return null
}

/**
 * Replaces current file with a re-uploaded file. Changes only
 * file type, file extension, file's size and modified date where
 * applicable.
 *
 * Firestore :
 * 1) /files/{file_id}
 *
 * Realtime :
 * 1) /shortLinks/{link_id}
 *
 * Storage :
 * 1) /{user_id}/{storage_id}
 *
 * @param {any}     firebase          firebase instance
 * @param {object}  fileObject        object with current file's data
 * @param {string}  fileStoragePath   firebase storage path for current file
 * @returns
 */
export async function replaceFile(firebase, fileObject, fileStoragePath) {
  if (!firebase || !fileObject || !fileStoragePath) {
    return
  }

  const { files, realtime, getCurrentTimestamp } = getDatabase()
  const fileStorageRef = getStorage().ref().child(fileStoragePath)
  const { file, owner, visibility, sharableUntil, id, shortLink } = fileObject
  let fileExtension = getFileExtension(file.name)

  let metaData = {
    customMetadata: {
      owner,
      visibility,
    },
  }

  if (sharableUntil) {
    metaData.customMetadata.sharableUntil = sharableUntil
  }

  // Cannot determine progress using async/await
  // Use .on to listen to changes
  await fileStorageRef.put(file, metaData)
  console.log("Re-placed file !")

  // Only update the data other than name in databases
  // NOTE: Even if the user uploads a different file type,
  // the name will be kept the same but it will be changed
  // in database, so its upto user to make sure they don't
  // do it. Otherwise just rename the file after replacing.
  await files.doc(id).update({
    size: file.size,
    fileType: file.type,
    fileExtension: fileExtension,
    modifiedAt: getCurrentTimestamp(),
  })

  const rdbUpdates = {}
  rdbUpdates[`search/${owner}/${id}/size`] = file.size

  if (shortLink) {
    rdbUpdates[`shortLinks/${shortLink}/fileExtension`] = fileExtension
    rdbUpdates[`shortLinks/${shortLink}/fileType`] = file.type
    rdbUpdates[`shortLinks/${shortLink}/size`] = file.size
  }

  await realtime.ref().update(rdbUpdates)
  return null
}
