import { getDatabase, getFirebase } from "../utils/firebase"

/**
 * Returns the data corresponding to a short link
 *
 * @param {any}     firebase      firebase instance
 * @param {string}  shortLinkId   generated short link
 * @returns object if exists otherwise null
 */
export async function getLinkData(firebase, shortLinkId) {
  if (!firebase || !shortLinkId) {
    // retry to get firebase instance if haven't got yet
    firebase = getFirebase()

    if (!firebase) {
      return
    }
  }

  const { realtime } = getDatabase()
  const shortLinkRef = await realtime
    .ref(`/shortLinks/${shortLinkId}`)
    .once("value")

  console.log("shortLinkRef => ", shortLinkRef)
  console.log("shortLinkRef.val() => ", shortLinkRef.val())

  return shortLinkRef.val()
}
