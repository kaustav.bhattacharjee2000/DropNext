import { getDatabase } from "../utils/firebase"

/**
 * Searches the firebase realtime db corresponding to
 * a user's collection (based on id). Note that it does
 * prefix search only.
 *
 * @param {any}     firebase    firebase instance
 * @param {string}  searchText  text to search (in lower case)
 * @param {string}  userId      user id for existing user
 * @returns array of search result objects
 */
export async function search(firebase, searchText, userId) {
  if (!firebase || !searchText || !userId) {
    return
  }

  const { realtime } = getDatabase()
  const searchPath = `search/${userId}`

  let resultObject = await realtime
    .ref(searchPath)
    .orderByChild("lower")
    .startAt(searchText)
    .endAt(searchText + "\uf8ff")
    .once("value")

  let resultArray = []
  resultObject.forEach(child => {
    let row = {}
    row[child.key] = child.toJSON()
    resultArray.push(row)
  })

  console.log("result => ", resultArray)
  return resultArray
}
