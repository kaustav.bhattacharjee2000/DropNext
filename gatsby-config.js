module.exports = {
  siteMetadata: {
    title: `Drop Next`,
    description: `A simplistic file storage webapp`,
    author: `Kaustav Bhattacharjee <kaustav.bhattacharjee2000@gmail.com>`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/assets`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `DropNext`,
        short_name: `DropNext`,
        start_url: `/`,
        background_color: `#e8ecef`,
        theme_color: `#018894`,
        display: `minimal-ui`,
        icon: `assets/icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: "gatsby-plugin-web-font-loader",
      options: {
        google: {
          families: ["Nunito Sans"],
        },
      },
    },
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /\.inline\.svg$/,
        },
      },
    },
    `gatsby-plugin-material-ui`,
    `gatsby-plugin-gatsby-cloud`,
    {
      resolve: "gatsby-plugin-remove-console",
      options: {
        exclude: ["error"], // <- will be removed all console calls except these
      },
    },
  ],
}
