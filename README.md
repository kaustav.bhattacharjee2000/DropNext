<div align="center">
  <p align="center">
    <img alt="DropNext icon" src="./assets/icon.png" width="60" align="center" />
  </p>
</div>
<div align="center">
  <h1 align="center">
    DropNext
  </h1>
</div>
A simple drive storage for people who want more control over their online stored files and folders. Currently in version 1.0.0 and provides the following functionalities : 
<p></p>

- [x] **Free to use** <br/>
      No charges for usage

- [x] **Easy sharing** <br/>
      Make any number of files as public and get their short links

- [x] **Expiration Date** <br/>
      Restrict your publicly shared files with a time-limited access

- [x] **File replace** <br />
      Replace any file without ever needing to change public link

Additionally you get all basic drive storage functionalities such as uploading files, creating folders, searching all files & folders etc. (_Has a **dark** mode theme too_ :smirk:)

Want to try the website ? [Go for it](https://dropnext.gatsbyjs.io)

Oh, you need to test it out locally ? Read along then.

## 🚀 Test locally

#### Installation

Since DropNext is built with Gatsby, you can go through the instructions on their [official docs](https://www.gatsbyjs.com/docs/tutorial/part-0/) to install gatsby-cli and run it in development mode.

Or, if you already have it installed, run the commands below :

```shell
yarn install
yarn dev
```

If you need to run it in production mode, run the commands below :

```shell
yarn prod
```

**Note** : If you open _package.json_ file to check **`dev`** script, you will notice that it will make the project available on your local network. Makes it easier to test on other devices :grin:.

#### Firebase

As the next and final step, create a [firebase](https://firebase.google.com/) project enabling the following :

- **Authentication** - email & password, google sign-in
- **Cloud firestore**

- **Realtime database**

Don't worry there are plenty of videos & tutorials on web if you get stuck. Copy the generated configs of your app in a `.env.development` file if starting out for development, or `.env.production` file if running in production mode. Make sure the environment variables are named as same as given in **firebaseConfig** variable of [**firebase.js**](./src/utils/firebase.js) file.

Finally, check your http://localhost:8000, or your local network ip on port 8000.

If running in production mode, change the port to **9000**.

**Note** : Keep the .env files in root folder. Gatsby will automatically use the .env file depending on the mode you're running the project. More info [here](https://www.gatsbyjs.com/docs/how-to/local-development/environment-variables/).

## 💫 Feedback

I've created it as a fun project, and will add new and cool features in the upcoming versions. If you have any feedback on the DropNext or some suggestions or anything else, feel free to ping me on [LinkedIn](https://www.linkedin.com/in/kaustavbhatt/).
